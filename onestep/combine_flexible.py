from __future__ import division
import numpy as np
import glob,h5py
import argparse, json
import pandas as pd
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="../../", help="path")
args = parser.parse_args()

'''
This code produced the training file (with resampling in pt and eta, without reweighting) from the reduced files.
source LCGsetup.sh beforehand
input files: Reduced{Dijets,Top,Hbb}

output: "../../Onestep/<file_name>.h5"

python combine_flexible.py --path ../../  2>&1 | tee comb_cor.txt
'''
def cut(df, max_col, min_sj=1, ptCut=3000, relDrMin=1.0, comments = False):
	# cut all events with pT>cut
	df.drop(df[df['pt'] >= ptCut].index, inplace=True)
	df.drop(df[df['pt'] <= 250].index, inplace=True)
	# cut on collinearity dR<1
	nRowOrig = len(df.index)
	if comments:
		print("Rows before subjet cuts: " + str(nRowOrig))
	
	if max_col==2: #drop fatjet if all subjets are collinear (missing subjet =nan is not collinear)
		df.drop(df[(df['relativeDeltaRToVRJet_1'] < relDrMin) & (df['relativeDeltaRToVRJet_2'] < relDrMin) & (df['relativeDeltaRToVRJet_3'] < relDrMin) ].index, inplace=True)
	elif max_col==1: #drop if 2 are col.
		df.drop(df[(df['relativeDeltaRToVRJet_1'] < relDrMin) & (df['relativeDeltaRToVRJet_2'] < relDrMin)].index, inplace=True)
		df.drop(df[(df['relativeDeltaRToVRJet_3'] < relDrMin) & (df['relativeDeltaRToVRJet_2'] < relDrMin)].index, inplace=True)
		df.drop(df[(df['relativeDeltaRToVRJet_1'] < relDrMin) & (df['relativeDeltaRToVRJet_3'] < relDrMin)].index, inplace=True)
	elif max_col==0: #drop if any collinear
		df.drop(df[(df['relativeDeltaRToVRJet_1'] < relDrMin) | (df['relativeDeltaRToVRJet_2'] < relDrMin) | (df['relativeDeltaRToVRJet_3'] < relDrMin) ].index, inplace=True)
	elif max_col==3:
		pass
	else:
		print('max_col has to be 0, 1, 2, or 3')
		exit()

	if comments:
		print("Rows ratio after collinear cut: " + str(len(df.index) / nRowOrig))
	
	if min_sj==1:
		#drop fatjets with 0 sj. assumption: if subjet_1 is missing (nan), then all subjets are missing. you can check this assumption with /plot_and_study/jet_properties
		df.drop(df[np.isnan(df['relativeDeltaRToVRJet_1'])].index, inplace=True)
	elif min_sj==2:
		#drop fatjets with 1 or 0 sj. assumption: if there are at least 2 subjets, subjet_2 is always there (not nan).
		df.drop(df[np.isnan(df['relativeDeltaRToVRJet_2'])].index, inplace=True)
	elif min_sj==3:
		#drop fatjets with 2 or 1 or 0 sj. assumption: if there are 3 subjet, subjet_3 is always there (not nan).
		df.drop(df[np.isnan(df['relativeDeltaRToVRJet_3'])].index, inplace=True)
	elif min_sj==0:
		pass
	else:
		print('min_sj has to be 0, 1, 2, or 3')
		exit()

	if comments:
		print("Rows ratio after subjet cut: " + str(len(df.index) / nRowOrig))

	#---------------------------------------------------------------------------------
	return df

def getvals(path,max_col,relDrMin=1.0):
	'''
	path: an h5 file path in a directory for the given process and dsid
	returns data to be appended to the output file
	
	edits: rename get_vars()
	'''
	# need to add eventNumber back
	#features_group1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','eventNumber','dsid','mass','pt','eta'] #0-9
	features_group1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','dsid','mass','pt','eta'] #0-9
	#features_group2=['DL1rT_pu_1', 'DL1rT_pc_1', 'DL1rT_pb_1','DL1rT_pu_2', 'DL1rT_pc_2', 'DL1rT_pb_2','DL1rT_pu_3', 'DL1rT_pc_3', 'DL1rT_pb_3']
	features_group2=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
	#features_group3=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
	features_group4=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20'] #27-41
	# features_group5=['JetFitter_N2Tpair_1', 'JetFitter_dRFlightDir_1', 'JetFitter_deltaeta_1', 'JetFitter_deltaphi_1', 'JetFitter_energyFraction_1', 'JetFitter_mass_1', 'JetFitter_massUncorr_1', 'JetFitter_nSingleTracks_1', 'JetFitter_nTracksAtVtx_1', 'JetFitter_nVTX_1', 'JetFitter_significance3d_1','SV1_L3d_1', 'SV1_Lxy_1', 'SV1_N2Tpair_1', 'SV1_NGTinSvx_1', 'SV1_deltaR_1', 'SV1_dstToMatLay_1', 'SV1_efracsvx_1', 'SV1_masssvx_1', 'SV1_significance3d_1', 'rnnip_pu_1', 'rnnip_pc_1', 'rnnip_pb_1', 'rnnip_ptau_1', 'JetFitter_N2Tpair_2', 'JetFitter_dRFlightDir_2', 'JetFitter_deltaeta_2', 'JetFitter_deltaphi_2', 'JetFitter_energyFraction_2', 'JetFitter_mass_2', 'JetFitter_massUncorr_2', 'JetFitter_nSingleTracks_2', 'JetFitter_nTracksAtVtx_2', 'JetFitter_nVTX_2', 'JetFitter_significance3d_2', 'SV1_L3d_2', 'SV1_Lxy_2', 'SV1_N2Tpair_2', 'SV1_NGTinSvx_2', 'SV1_deltaR_2', 'SV1_dstToMatLay_2', 'SV1_efracsvx_2', 'SV1_masssvx_2', 'SV1_significance3d_2',  'rnnip_pu_2', 'rnnip_pc_2', 'rnnip_pb_2', 'rnnip_ptau_2', 'JetFitter_N2Tpair_3', 'JetFitter_dRFlightDir_3', 'JetFitter_deltaeta_3', 'JetFitter_deltaphi_3', 'JetFitter_energyFraction_3', 'JetFitter_mass_3', 'JetFitter_massUncorr_3', 'JetFitter_nSingleTracks_3', 'JetFitter_nTracksAtVtx_3', 'JetFitter_nVTX_3', 'JetFitter_significance3d_3', 'SV1_L3d_3', 'SV1_Lxy_3', 'SV1_N2Tpair_3', 'SV1_NGTinSvx_3', 'SV1_deltaR_3', 'SV1_dstToMatLay_3', 'SV1_efracsvx_3', 'SV1_masssvx_3', 'SV1_significance3d_3', 'rnnip_pu_3', 'rnnip_pc_3', 'rnnip_pb_3', 'rnnip_ptau_3'] #41-113

	features=features_group1+features_group2+features_group4

	store = pd.HDFStore(path)
	df = store.get("dataset")[features] #runs on files from Reduced*
	store.close()
	#df['dsid']=pd.to_numeric(df['dsid'])

	# nRowOrig = len(df.index)
	# print("Rows: " + str(nRowOrig))

	df = cut(df,max_col=max_col)
	# print("Remaining : " + str(float(len(df.index)/float(nRowOrig))))

	print("Flag overlapping VR jets")
	# if relativeDeltaRToVRJet is < 1, flavor labeling is unreliable so set to nan
	#set it to 1/3 (no prior knowledge) instead of nan?

	df[['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1']] = df[['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1']].where(df['relativeDeltaRToVRJet_1'] > relDrMin, other=np.nan)
	df[['DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2']] = df[['DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2']].where(df['relativeDeltaRToVRJet_2'] > relDrMin, other=np.nan)
	df[['DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3']] = df[['DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3']].where(df['relativeDeltaRToVRJet_3'] > relDrMin, other=np.nan)

	# print("Calculate DL1r variables")
	#Add baseline DL1r values to a new column for each subjet
	f=0.018
	subjets = ['1', '2', '3']
	# loop over indices and build variable
	for sj in subjets:
		# print("\t subject " + sj)
		#not using these vars in training, but still want to keep them in file to have the correct number of columns. Therefore, I set all of them to 0 to save computation time. A better fix later.
		df['DL1r_'+sj] = 0 
		# df['DL1r_'+sj] = df.apply(lambda row: row['DL1r_pb_'+sj] / ( (1-f)*row['DL1r_pu_'+sj] + f*row['DL1r_pc_'+sj] ), axis=1)

		# #Replace nan with small number
		# df[['DL1r_'+sj]] = df[['DL1r_'+sj]].where( ~np.isnan(df['DL1r_'+sj]), other=1e-15)

		# #Replace inf with small number
		# df[['DL1r_'+sj]] = df[['DL1r_'+sj]].where( ~np.isinf(df['DL1r_'+sj]), other=1e-15)

		# #Log and clip ... following the original 
		# df[['DL1r_'+sj]] = np.log(np.clip( df[['DL1r_'+sj]], 1e-30, 1e30))

		df['DL1rBin_'+sj] = 0
		# #Add a binned DL1r - copy code from above - terrible
		# df['DL1rBin_'+sj] = df.apply(lambda row: row['DL1r_pb_'+sj] / ( (1-f)*row['DL1r_pu_'+sj] + f*row['DL1r_pc_'+sj] ), axis=1)

		# #Replace nan with nan...? Following exactly Wei's logic but seems redundant
		# df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( ~np.isnan(df['DL1rBin_'+sj]), other=np.nan)

		# #Replace inf with nan
		# df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( ~np.isinf(df['DL1rBin_'+sj]), other=np.nan)

		# #Is there a more compact way of doing thins?
		# df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  4.805, other=5 )
		# df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  3.515, other=4 )
		# df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  2.585, other=3 )
		# df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] <  1.085, other=2 )
		# df[['DL1rBin_'+sj]] = df[['DL1rBin_'+sj]].where( df['DL1rBin_'+sj] >= 1.085, other=1 )

	return df.values

def find_resample_info(paths,max_col, minpt=250, maxpt=3000, ptwidth = 10, netabins = 1000):
	'''
	paths: list of paths of all files being processed.
	returns a probability histogram of each process for resampling.
	returns histogram bins.
	'''
	nptbins = int((maxpt - minpt) / 10) #number of bins of width 10 GeV
	ptbins, etabins = np.arange(int(minpt), int(maxpt) + 10, ptwidth), np.linspace(-2, 2, netabins+1) #so you get bins of 10 GeV, and netabins bins in eta
	bins = [ptbins, etabins]
	h_hist, d_hist, t_hist = np.zeros((nptbins,netabins)), np.zeros((nptbins,netabins)), np.zeros((nptbins,netabins))
	j = 0
	hdt_beforecut = [0,0,0]
	#features needed for cuts:
	features=['relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','pt','eta']
	for f in paths:
		print('finding hists ' + str(j+1))
		j += 1
		store = pd.HDFStore(f)
		df = store.get("dataset")[features] #runs on files from Reduced*
		store.close()
		# get stats
		nRowOrig = len(df.index)

		if 'Hbb' in f:
			hdt_beforecut[0] += nRowOrig
		elif 'Dijets' in f:
			hdt_beforecut[1] += nRowOrig
		elif 'Top' in f:
			hdt_beforecut[2] += nRowOrig
		#---------------------------------------------------------------------------------
		#cut first:
		df = cut(df,max_col=max_col, comments = False)
		#---------------------------------------------------------------------------------
		pt = df['pt']
		eta = df['eta']
		# if min(pt) < 250:
		# 	print('minimum is lower than expected. Lower the binning limit.')
		# 	print(f)
		# 	exit()
		
		hist, _, _ = np.histogram2d(pt, eta, bins) #2d histogram in pt and eta bins
		
		Top=False
		if "Hbb" in f:
			h_hist += np.array(hist)
			print('found a Higgs!')
			# print(h_hist, d_hist, t_hist)
		elif "Dijets" in f:
			d_hist += np.array(hist)
			print('found a Di!')
			# print(h_hist, d_hist, t_hist)
		elif "Top" in f:
			Top = True
			t_hist += np.array(hist)
			print('found a Top!')
			# print(h_hist, d_hist, t_hist)
	# if 0 in h_hist:
	#     print('found zero in Hbb')
	#     print(h_hist)
	#     exit()
	# if 0 in d_hist:
	#     print('found zero in Dijets')
	#     print(d_hist)
	#     exit()
	# # if 0 in t_hist:
	# #     print('found zero in Top')
	# #     print(t_hist)
	# #     exit()
	# print('stats after cuts: Hbb, Dijets, Top:')
	# print(np.sum(h_hist), np.sum(d_hist), np.sum(t_hist))
	hdt_aftercut = [np.sum(h_hist), np.sum(d_hist), np.sum(t_hist)]

	if Top:
		background = np.minimum(d_hist, t_hist)
	else:
		background = d_hist
	# print(h_hist, d_hist, t_hist)

	target = np.minimum(h_hist, background)


	h_prob = target / h_hist
	d_prob = target / d_hist
	t_prob = target / t_hist

	return np.nan_to_num(h_prob), np.nan_to_num(d_prob), np.nan_to_num(t_prob), bins, hdt_beforecut, hdt_aftercut

def resample2d(data, path, h_prob, d_prob, t_prob, bins):
	if 'Hbb' in path:
		prob_hist = h_prob
	elif 'Dijets' in path:
		prob_hist = d_prob
	elif 'Top' in path:
		prob_hist = t_prob
	# print('prob_hist.shape:')
	# print(prob_hist.shape)
	pt=data[:,6]
	eta=data[:,7]
	pt_index=np.array(np.digitize(pt,bins[0])-1) #np.digitize(pt,bins) returns the indices of the bins to which each value pt belongs. first bin is 1.
	eta_index=np.array(np.digitize(eta,bins[1])-1)
	# print('pt.shape:')
	# print(pt.shape)
	# print('eta.shape:')
	# print(eta.shape)
	# print('max index:')
	# print(np.max(pt_index))
	# print(np.max(eta_index))
	# print('min index:')
	# print(np.min(pt_index))
	# print(np.min(eta_index))
	# print('bins')
	# print(len(bins[0]))
	# print(len(bins[1]))
	mask = prob_hist[pt_index, eta_index] >= np.random.uniform(0,1.0,pt.shape)

	return data[mask]

def label(in_data, in_file):
	'''
	in_file: path of the file being processed.
	in_data: array?: coming from getvals()
	this function replaces prepare.py
	add a vector to each jet to lable its process.
	'''
	out_data = in_data
	# REWEIGHT so average weight = 1 in smallest sample (we think it's Hbb), rest match integral
	if "Dijets" in in_file:
		load_y=np.full((in_data.shape[0],3),[1.,0.,0.]) #matrix of shape Nx3, with the first row filled with 1. 
	elif "Hbb" in in_file:
		load_y=np.full((in_data.shape[0],3),[0.,1.,0.]) #matrix of shape Nx3, with the second row filled with 1
	elif "Top" in in_file:
		load_y=np.full((in_data.shape[0],3),[0.,0.,1.]) #matrix of shape Nx3, with the third row filled with 1. 
	# add the array to the list of 37 keys.
	out_data=np.hstack((in_data,load_y))
	# in_data is Nx37, out_data is Nx(37+3), each row is a jet. each colomn is jet variables, and the last 3 elements of each colomn tell you if it's Dijets, Hbb, or Top.
	
	return out_data

def standardize(data):
	"""
	data: array of jet rows and jet variables colonms
	"""
	print("Scaling data")
	print(data.shape)
	# pick up from pt (6) to before the categories
	tVars=data[:,6:-3]
	# calculate mean and std of each variable
	mean_vector = np.nanmean(tVars, axis=0)
	std_vector = np.nanstd(tVars, axis=0)

	Info=data[:,0:6] # last number is NOT included
	cats=data[:,-3:]
	if data.shape[1] != (Info.shape[1] + tVars.shape[1] + cats.shape[1]):
		print("PROBLEM")
		exit()
	elif tVars.shape[1] != len(mean_vector):
		print("mean_vector length not matched")
		exit()
	elif tVars.shape[1] != len(std_vector):
		print("std_vector length not matched")
		exit()
	tVars=(tVars-mean_vector)/std_vector

	scaled_data=np.hstack((Info,tVars,cats))
	return scaled_data, mean_vector, std_vector

def splitdata(Data, SaveFile, create_file = False):
	'''
	Data: array?: data in one file.
	SaveFile: h5 file to be filled with new data.

	This function splits data for each file into three catagories (cats): ["train","test","valid"]

	creates dataset in SaveFile and appends new data to it,
	then appends new data to the created file in another iteration
	'''
	save_f = h5py.File(SaveFile, 'a')
	N=Data.shape[0]
	print("Input rows: " + str(N))
	train_index=np.random.choice(N,int(0.7*N),replace=False) # array containing 70% of N integers
	remain_index=np.setdiff1d(np.arange(0,N),train_index)  # array containing all integers < N and different from integers in train_index
	nRemain=len(remain_index)
	valid_index=np.random.choice(remain_index,int(0.5*nRemain),replace=False) #50% of remaining 30% = 15%
	test_index=np.setdiff1d(remain_index,valid_index)
	catindex={"train":train_index,"test":test_index,"valid":valid_index}

	for cat, index in catindex.items():
		cat_data=np.take(Data,index,axis=0)

		cat_data=np.nan_to_num(cat_data) #training doesn't work without it, but it looks problimatic to turn all nans to 0.

		#append data to file:
		if create_file:
			print("Create dataset : " + cat)
			save_f.create_dataset(cat,data=cat_data, maxshape=(None, cat_data.shape[1]), chunks=True)
		elif cat_data.shape[0] != 0:
			save_f[cat].resize((save_f[cat].shape[0] + cat_data.shape[0]), axis = 0)
			# add Data to its catagory
			save_f[cat][-cat_data.shape[0]:] = cat_data
			print(save_f[cat].shape)
		else:
			print('empty dataset found!')

def workflow(paths, SaveFile):
	j=0
	for Path in paths:
		j+=1
		print('handling file number ' + str(j))
		print(Path)
		
		data = getvals(Path, max_col=0)
		
		data = resample2d(data, Path, h_prob, d_prob, t_prob, bins)
		data = label(data, Path)
		if j == 1:
			Data = data #if this doesn't work due to lxplus memory, do the line below instead, then use the standardize in another step.
			# splitdata(data, SaveFileNonStandardized, create_file=True)
		else:
			Data = np.concatenate((Data, data), axis=0) #if this doesn't work due to lxplus memory, do the line below instead, then use the standardize in another step using std_from_file.
			# splitdata(data, SaveFileNonStandardized)

	# f = '/eos/user/o/okarkout/full_run_v3/Onestep/train_flex_0col.h5'
	# store = pd.HDFStore(f)
	# store=h5py.File('/eos/user/o/okarkout/full_run_v3/Onestep/train_flex_0col.h5',"r")
	# train = store.get("train")[:,:]
	# valid = store.get("valid")[:,:]
	# test = store.get("test")[:,:]
	# store.close()
	# Data=np.concatenate((train,valid,test), axis = 0)

	Data, mean, std = standardize(Data)

	#save the scaling info:
	SaveName = SaveFile.split('/')[3].split('.')[0]
	a_file = open("scale_info_"+SaveName+".json", "w")
	json.dump({'mean': mean.tolist(), 'std': std.tolist()}, a_file)
	a_file.close()

	splitdata(Data, SaveFile, create_file=True)

	print('total jets after preprocessing = ', len(Data))

def std_from_file(path, SaveFile):
	"""
	takes a training file which was not standardized, and produces a standardized file from its data.
	"""
	trainfile = h5py.File(path,"r")
	train = trainfile.get('train')
	valid = trainfile.get('valid')
	test = trainfile.get('test')
	Data = np.concatenate((train, valid, test), axis=0)

	Data, mean, std = standardize(Data)

	#save the scaling info:
	SaveName = SaveFile.split('/')[3].split('.')[0]
	a_file = open("scale_info_"+SaveName+".json", "w")
	json.dump({'mean': mean.tolist(), 'std': std.tolist()}, a_file)
	a_file.close()

	splitdata(Data, SaveFile, create_file=True)

if __name__ == "__main__":
	# dpaths=sorted(glob.glob(args.path+"/ReducedDijets/*361029*/*.h5"))
	# paths=sorted(glob.glob(args.path+"/ReducedHbb/*309450*/*.h5")) #testmode
	# paths = dpaths + hpaths
	# paths = sorted(glob.glob(args.path+"/Reduced*/*/*.h5"))
	# print("number of files is ", len(paths))
	# SaveFileNonStandardized = h5py.File("../../Onestep/train_flex_0col_v2.h5", 'a')
	# SaveFileNonStandardized2 = h5py.File("../../Onestep/train_flex_2col.h5", 'a')
	# NoStdFile = "../../Onestep/trainstd_aug25_0col.h5"
	SaveFile = "../../Onestep/trainstd_aug25_std_corrected_0col.h5"

	h_prob, d_prob, t_prob, bins, hdt_beforecut, hdt_aftercut = find_resample_info(paths,max_col=0)
	print('hdt_beforecut, hdt_aftercut')
	print(hdt_beforecut, hdt_aftercut)

	#save the resampling info:
	a_file = open("resample_info_oct_0col.json", "w")
	json.dump({'h_prob': h_prob.tolist(), 'd_prob': d_prob.tolist(), 't_prob': t_prob.tolist(), 'bins': [bins[0].tolist(), bins[1].tolist()], 'hdt_beforecut': hdt_beforecut, 'hdt_aftercut': hdt_aftercut}, a_file)
	a_file.close()

	########################################################################
	# a_file = open("resample_info_flex_0col.json", "r")
	# info_str = a_file.read()
	# a_file.close()
	# info = json.loads(info_str)
	# h_prob, d_prob, t_prob, bins, hdt_beforecut, hdt_aftercut = np.array(info['h_prob'], dtype=object), np.array(info['d_prob'], dtype=object), np.array(info['t_prob'], dtype=object), np.array(info['bins'], dtype=object), np.array(info['hdt_beforecut'], dtype=object), np.array(info['hdt_aftercut'], dtype=object)
	
	workflow(paths, SaveFile)

	# std_from_file(NoStdFile, SaveFile)
	
#unused:
# def find_weights(data):
#     '''
#     data is array: getvalsed resampled data.
#     this function returns the sum of weights for all files for processes [Hbb, Top, Dijets]
#     returns the number of Hbb Jets
#     '''

#     print('calculating weights')
#     weights = [0,0,0] # these are the integral weights of [Hbb,Top,Dijets]
#     nJetsHbb = 0

#     #---------------------------------------------------------------------------------
#     #now calculate the sum of weights
#     for jet in data:
#         w = jet[0]
#         if int(jet[-2]) == 1: #Hbb
#             weights[0] += w # save the weight
#             nJetsHbb += 1
#         if int(jet[-1]) == 1:
#             weights[1] += w # save the weight
#         if int(jet[-3]) == 1:
#             weights[2] += w # save the weight
	
#     return weights, nJetsHbb

# def reweight(in_data, weights, nJetsHbb):
#     '''
#     weights: list on int: integral weights of [Hbb,Top,Dijets]
#     nJetsHbb: int: overall number of Hbb jets.
#     in_data: np array: coming from label

#     reweight the data such that the sum of weights of each process is equal to the number of Hbb jets.
#     '''
#     out_data = in_data
#     i=0
#     for jet in in_data:
#         if jet[-3] == 1:
#             out_data[i,0] = in_data[i,0] * nJetsHbb/ weights[2] # now the sum of Dijet weights = nJetsHbb
#         if jet[-2] == 1:
#             out_data[i,0] = in_data[i,0] * nJetsHbb/ weights[0]
#         if jet[-1] == 1:
#             out_data[i,0] = in_data[i,0] * nJetsHbb/ weights[1] # now the sum of Top weights = nJetsHbb
#         i += 1
	
#     return out_data
# weights, nJetsHbb = find_weights(Data)
# print('weights, nJetsHbb')
# print(weights, nJetsHbb)
# Data = reweight(Data, weights, nJetsHbb)
# print('data reweighted')