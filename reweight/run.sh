source ../setup_xbb.sh
#Calculate weights for dijets samples
path="path to dijets samples"
python calculatedDijetsWeights.py --path $path

#Get VR Ghost jets 
python labelDijetsDatasets.py --path $pathToDijets
python labelHbbDatasets.py --path $pathToHbb
python labelTopDatasets.py --path $pathToTop
