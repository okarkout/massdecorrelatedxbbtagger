import pandas as pd
import glob,math,os,h5py
import numpy as np

import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
#parser.add_argument("--outname", dest='outname',  default="", help="outname")
args = parser.parse_args()
path1=args.path
print('checking file {}'.format(path1))

# f=h5py.File(path1)
# data=f.get('train')
# data=f.get('predict')
# data=f.get('test')
# data=f.get('metadata')
# print(data)
# print(data[:,:])
#print f["metadata"]["nEventsProcessed"].shape
# hdf = pd.HDFStore(path1)
# fj = hdf.get("fat_jet")['mcEventWeight']
#h1=pd.read_hdf(path1,"subjet_VRGhostTag_1")

# h1 = pd.read_hdf(path1, mode = 'r') #files in /Reduced*
features_group1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','dsid','mass','pt','eta'] #0-9
#features_group2=['DL1rT_pu_1', 'DL1rT_pc_1', 'DL1rT_pb_1','DL1rT_pu_2', 'DL1rT_pc_2', 'DL1rT_pb_2','DL1rT_pu_3', 'DL1rT_pc_3', 'DL1rT_pb_3']
features_group2=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
#features_group3=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
features_group4=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20'] #27-41
features_group5=['JetFitter_N2Tpair_1', 'JetFitter_dRFlightDir_1', 'JetFitter_deltaeta_1', 'JetFitter_deltaphi_1', 'JetFitter_energyFraction_1', 'JetFitter_mass_1', 'JetFitter_massUncorr_1', 'JetFitter_nSingleTracks_1', 'JetFitter_nTracksAtVtx_1', 'JetFitter_nVTX_1', 'JetFitter_significance3d_1','SV1_L3d_1', 'SV1_Lxy_1', 'SV1_N2Tpair_1', 'SV1_NGTinSvx_1', 'SV1_deltaR_1', 'SV1_dstToMatLay_1', 'SV1_efracsvx_1', 'SV1_masssvx_1', 'SV1_significance3d_1', 'rnnip_pu_1', 'rnnip_pc_1', 'rnnip_pb_1', 'rnnip_ptau_1', 'JetFitter_N2Tpair_2', 'JetFitter_dRFlightDir_2', 'JetFitter_deltaeta_2', 'JetFitter_deltaphi_2', 'JetFitter_energyFraction_2', 'JetFitter_mass_2', 'JetFitter_massUncorr_2', 'JetFitter_nSingleTracks_2', 'JetFitter_nTracksAtVtx_2', 'JetFitter_nVTX_2', 'JetFitter_significance3d_2', 'SV1_L3d_2', 'SV1_Lxy_2', 'SV1_N2Tpair_2', 'SV1_NGTinSvx_2', 'SV1_deltaR_2', 'SV1_dstToMatLay_2', 'SV1_efracsvx_2', 'SV1_masssvx_2', 'SV1_significance3d_2',  'rnnip_pu_2', 'rnnip_pc_2', 'rnnip_pb_2', 'rnnip_ptau_2', 'JetFitter_N2Tpair_3', 'JetFitter_dRFlightDir_3', 'JetFitter_deltaeta_3', 'JetFitter_deltaphi_3', 'JetFitter_energyFraction_3', 'JetFitter_mass_3', 'JetFitter_massUncorr_3', 'JetFitter_nSingleTracks_3', 'JetFitter_nTracksAtVtx_3', 'JetFitter_nVTX_3', 'JetFitter_significance3d_3', 'SV1_L3d_3', 'SV1_Lxy_3', 'SV1_N2Tpair_3', 'SV1_NGTinSvx_3', 'SV1_deltaR_3', 'SV1_dstToMatLay_3', 'SV1_efracsvx_3', 'SV1_masssvx_3', 'SV1_significance3d_3', 'rnnip_pu_3', 'rnnip_pc_3', 'rnnip_pb_3', 'rnnip_ptau_3'] #41-113
features=features_group1+features_group2+features_group4
print(features)
# h2 = pd.read_hdf(path1, columns=features) #files in /Reduced*

store = pd.HDFStore(path1)
h2 = store.get("dataset")[features]
store.close()

# nRowOrig = len(h2.index)
# print("Rows: " + str(nRowOrig))

# # cut all events with pT>cut
# ptCut=1000
# print("Cutting on jet pT < " + str(ptCut))

# h2.drop(h2[h2['pt'] > ptCut].index, inplace=True)
# print("Remaining : " + str(float(len(h2.index)/float(nRowOrig))))

# w = h2["weight"]
# w = store.get("dataset")["weight"]
# print(w[1])
# store.close()
# x = 0
# for n in range(len(w)):
#     x += w[n]
# print(x)
# print(np.sum(w))
# h1=pd.read_hdf(path1,"fat_jet")
# h1=pd.read_hdf(path1,"metadata")
# print list(h1.keys())
# print h1.keys
# for i in h1.keys():
#     print i

print('file looks ok {}\n'.format(path1))


    
