import os, glob
import argparse
import numpy as np
import pandas as pd
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib import pyplot as plt



def get_hist(ds, edges):
    hist = 0
    files=glob.glob(ds + "*.h5")
    for fpath in files:
        print(fpath)
        df = pd.read_hdf(fpath,"dataset")
        pt     = df['pt']
        weight = df['weight']
        hist += np.histogram( pt, edges, weights=weight )[0]
        if np.any(np.isnan(hist)):
            print(fpath + " has NANs\n")
        print(hist)
        return hist

def draw_hist(hist, edges, out_dir, parts={}, file_name='dijet_pT.pdf'):
    #helvetify()
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    centers = (edges[1:] + edges[:-1]) / 2
    gev_per_bin = (centers[2] - centers[1])

    fig    = Figure((10,8))
    fig.set_tight_layout(True)
    canvas = FigureCanvas(fig)
    ax     = fig.add_subplot(1,1,1)


    ax.plot(centers, hist)
    #ax.bar(centers, hist)
    ax.set_yscale('log')
    #ax.set_ylabel(f'jets * fb / {gev_per_bin:.0f} TeV', ha='right', y=0.98, fontsize=20)
    #ax.set_xlabel(r'Fat Jet $p_{\rm T}$ [TeV]', ha='right', x=0.98, fontsize=20)
    maxval = ax.get_ylim()[1]
    #ax.set_ylim(0.1, maxval)
    #for dsid, part in parts.items():
    #    ax.plot(centers, part, label=get_pretty_name(dsid, 'terse'))
    ax.legend(ncol=2)
    canvas.print_figure(out_dir + "/" + file_name, bbox_inches='tight')



def run():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--path',   dest='path',   default='../../ReducedDijets/')
    parser.add_argument('--output', dest='output', default='pt-hists')
    args = parser.parse_args()

    edges = np.concatenate([[-np.inf], np.linspace(250, 3e3, 51), [np.inf]]) 
    out_hists = os.path.join(args.output, 'jetpt.h5')
    if os.path.isfile(out_hists):
        os.remove(out_hists)

    # get a hist per dsid
    #disds = [361023, 361024, 361025, 361026, 361027, 361028]
    #disds = [361024, 361025, 361026, 361027, 361028, 361029]
    disds = [361024, 361025]
    #disds = [361024]
    hists = []
    total = 0
    for dsid in disds:
        print("BEFORE total")
        print(total)
        print("BEFORE hist")
        print(hist)
        hist = get_hist(args.path+ "/*" + str(dsid) + "*/", edges)
        print("AFTER hist")
        print(hist)
        total += hist
        print("AFTER total")
        print(total)
        filename = "dijet_pT_" + str(dsid) + ".pdf"
        draw_hist(hist, edges, "/eos/user/o/okarkout/plots", parts={}, file_name=filename)
        hists.append(hist)
    #print("hists")
    #print(hists)

    print("done total")
    print(total)

    draw_hist(hist, edges, "/eos/user/g/gfacini/work/HbbTagger/", parts={}, file_name="dijet_pT.pdf")



if __name__ == '__main__':
    run()


