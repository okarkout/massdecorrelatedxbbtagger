import argparse
import os,glob,h5py,shutil
# import tensorflow as tf
from keras import backend as K
import keras as keras
from keras.models import Model, Sequential
from keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from keras.optimizers import SGD,Adam
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
#from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages



def plot_loss(history, withPt):
  f = plt.figure()
  plt.plot(history.history['loss'], label='loss')
  plt.plot(history.history['val_loss'], label='val_loss')
#  if not withPt:
#     plt.ylim([0, 2])
#  else:
#     plt.ylim([0,10])
  plt.xlabel('Epoch')
  plt.ylabel('Loss')
  plt.yscale('log')
  plt.legend()
  plt.grid(True)
  f.savefig("loss.png", bbox_inches='tight')
  print('plot_loss called')
  return f



def define_model(params):
    kinematic_input = Input(shape=(2, ), name='fatjet') # increase if adding JSS variables
    subjet_1_input  = Input(shape=(3, ), name='subjet0') 
    subjet_2_input  = Input(shape=(3, ), name='subjet1') 
    subjet_3_input  = Input(shape=(3, ), name='subjet2')        
    inputs = [kinematic_input, subjet_1_input, subjet_2_input, subjet_3_input]
    concatenated_inputs = keras.layers.concatenate(inputs)
    for i in range(params['num_layers']):
        if i==0:
            x = Dense(params['num_units'], kernel_initializer='orthogonal')(concatenated_inputs)
            if params['batch_norm']:
               x = BatchNormalization()(x)
            x = Activation(params['activation_type'])(x)
            if params['dropout_strength'] > 0:
               x = Dropout(params['dropout_strength'])(x)
        else:
            x = Dense(params['num_units'], kernel_initializer='orthogonal')(x)
            if params['batch_norm']:
               x = BatchNormalization()(x)
            x = Activation(params['activation_type'])(x)
            if params['dropout_strength'] > 0:
               x = Dropout(params['dropout_strength'])(x)

    predictions = Dense(params['output_size'], activation='softmax', kernel_initializer='orthogonal')(x)
    model = Model(inputs=inputs, outputs=predictions)
   #  adm = Adam(lr=params['learning_rate'], decay=params['lr_decay'])
    adm = Adam(learning_rate=params['learning_rate'], decay=params['lr_decay'])

    model.compile(loss='categorical_crossentropy', optimizer=adm)
    return model


print("************ PHASE LOAD FILE *****************")
train_file=h5py.File("./trainstd_oct_0col.h5","r") #need this file as import

# THIS IS TERRIBLE - get the input features with magic numbers....flatten step is a total waste
# lenght is 40 as package is setup now
train=train_file.get("train")
valid=train_file.get("valid")
print("train.shape")
print(train.shape)
#print("rows")
#print(len(train))

# feed the training what is described in the note --- magic numbers are terrible!
# weight position 0
# jet pT position 6 
# jet eta position 7
# subjet pb, pc, pl x 3 : 8-16
fatjet =train[:, [ 6, 7]] # add JSS variables here AFTER 7 (and below in valid)
subjet0=train[:, [ 8, 9,10]]
subjet1=train[:, [11,12,13]]
subjet2=train[:, [14,15,16]]
#print(fatjet[:5])
#print(fatjet[-5:])
# train_y=train[:, [37,38]] # get last 2 categories - not using top
train_y=train[:, [37,38,39]] # get last 3 categories - using top
#print(train_y[:5])
#print(train_y[-5:])
# train_w=train[:,0] # use weight - should RW to flat distribution in pT

# validation data
fatjetV =valid[:, [ 6, 7]] # add JSS variables here AFTER 7
subjet0V=valid[:, [ 8, 9,10]]
subjet1V=valid[:, [11,12,13]]
subjet2V=valid[:, [14,15,16]]
# valid_y=valid[:,[37,38]] #if no top
valid_y=valid[:,[37,38,39]]
# valid_w=valid[:,0] # use weight - should RW to flat distribution in pT


print("************ PHASE TRAINING *****************")
nEpochs = 100

# params = {'num_layers': 6,'num_units': 250,'activation_type': 'relu','dropout_strength': 0.,'learning_rate': 0.01,'lr_decay': 0.00001,'epochs': nEpochs,'batch_norm': True,'output_size': 2,} # output size is only 2 since not using top samples
params = {'num_layers': 4,'num_units': 50,'activation_type': 'relu','dropout_strength': 0.,'learning_rate': 0.01,'lr_decay': 0.00001,'epochs': nEpochs,'batch_norm': True,'output_size': 3,} # output size is only 3 since using top samples

model = define_model(params)
model_name ="trainstd_oct_100E_0col_6_250"
print(model_name)
save_path = "./"
batchsize=10000
save_best = keras.callbacks.ModelCheckpoint(filepath=save_path + model_name + "_best.h5", monitor='val_loss', verbose=0, save_best_only=True)
early_stopping = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=20)
csv_logger = keras.callbacks.CSVLogger(save_path + model_name + '.log')
reduce_lr_on_plateau = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=10, verbose=0, mode='auto', epsilon=0.0001, cooldown=0, min_lr=0)
callbacks = [save_best, early_stopping, csv_logger, reduce_lr_on_plateau]
# callbacks = [save_best, csv_logger, reduce_lr_on_plateau]

'weighted training:'
# history = model.fit(x=[fatjet,subjet0,subjet1,subjet2],y=train_y,sample_weight=train_w,validation_data=([fatjetV,subjet0V,subjet1V,subjet2V],valid_y,valid_w),batch_size=batchsize,callbacks=callbacks,epochs = params['epochs'])
'unweighted training:'
history = model.fit(x=[fatjet,subjet0,subjet1,subjet2],y=train_y,validation_data=([fatjetV,subjet0V,subjet1V,subjet2V],valid_y),batch_size=batchsize,callbacks=callbacks,epochs = params['epochs'])

print("History")
print(history)

arch = model.to_json()
with open(save_path + model_name + '_architecture.json', 'w') as arch_file: #output
        arch_file.write(arch)
model.save_weights(save_path + model_name + '_weights.h5') #output
model.save(save_path + model_name + '.h5') #output
print('model saved')
pdf = PdfPages("training_"+model_name+".pdf") #output
lossPlot = plot_loss(history,False)
pdf.savefig( lossPlot )  #output
pdf.close()
print('loss plot generated')