import glob
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

def get_hist(path, dsid):
	hist = 0
	files=glob.glob(path + "/*" + str(dsid) + "*/*.h5")
	if files == []:
		print('did not find files')
		exit()
	for fpath in files:
		print(fpath)

		store = pd.HDFStore(fpath)
		weights = store.get("dataset")["weight"]
		pt     = store.get("dataset")["pt"]
		store.close()

		#if you want to plot unweighted:
		weights = np.ones(len(weights))

		bin_edges = np.arange(0, 4500, 100)
		histo, bins = np.histogram(pt, bins=bin_edges, weights=weights)
		hist += histo

		errors = []
		bin_centers = []

		for bin_index in range(len(bins) - 1):

			# find which pt points are inside this bin
			bin_left = bins[bin_index]
			bin_right = bins[bin_index + 1]
			in_bin = np.logical_and(bin_left < pt, pt <= bin_right)

			# filter the weights to only those inside the bin
			weights_in_bin = weights[in_bin]

			# compute the error however you want
			error = np.sqrt(np.sum(weights_in_bin ** 2))
			errors.append(error)

			# save the center of the bins to plot the errorbar in the right place
			bin_center = (bin_right + bin_left) / 2
			bin_centers.append(bin_center)

		if np.any(np.isnan(histo)):
			print(fpath + " has NANs\n")
	# print(hist)
	return hist, bin_centers, errors

def draw_hist(hist, bin_centers, errors, out_dir, file_name):

	plt.errorbar(bin_centers, hist, yerr = errors, linewidth=1, ecolor='r', capsize=2, marker = '.', drawstyle = 'steps-mid')
	plt.yscale('log')
	plt.ylabel(file_name, ha='right', y=0.98, fontsize=10)
	plt.xlabel('pt in GeV', ha='right', x=0.98, fontsize=10)

	plt.savefig(out_dir + "/hist_" + file_name + "_pT.pdf")

def draw_3_hist(histlist, xlabel, out_dir, file_name):
	i = 0
	colors = ['m', 'tab:blue', 'g']
	# colors = ['g', 'b', 'crimson']
	labels = ['DiJets', 'Hbb', 'Top']
	linestyles = ['dotted', 'dashed', 'dotted']
	transparency = [0.7, 0.7, 0.7]
	linewidths = [1.5, 1.5, 1.5]
	for item in histlist:
		print(i)
		if i < 5:
			hist, bin_centers, errors = item[0], item[1], item[2]
			plt.errorbar(bin_centers, hist, yerr = errors, linewidth=linewidths[i], color = colors[i], label = labels[i], ecolor='r', capsize=2, marker = '', drawstyle = 'steps-mid')
			# plt.step(x=bins, y=hist, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
			# plt.stairs(hist, edges=bins, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		i += 1
	plt.legend(loc="upper right", fontsize='x-small')
	plt.yscale('log')
	# plt.title(file_name, ha='right', y=0.98, fontsize=10)
	# plt.xlabel('eta', ha='right', x=0.98, fontsize=10)
	plt.xlabel(xlabel, ha='right', x=0.98, fontsize=10)
	# plt.xlabel('DL1r', ha='right', x=0.98, fontsize=10)
	plt.savefig(out_dir + "/hist_" + file_name + "_" + xlabel + ".pdf") #
	plt.close()

def run():
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('--path1',   dest='path1',   default='../../ReducedDijets/')
	parser.add_argument('--path2',   dest='path2',   default='../../ReducedHbb/')
	# parser.add_argument('--path',   dest='path',   default='../../ReducedTop/')
	args = parser.parse_args()

	# get a hist per dsid
	# dsids = [361020, 361021, 361022, 361023, 361024, 361025, 361026, 361027, 361028, 361029, 361030, 361031]
	# dsids = [361023, 361028]
	dsids = [] #run on all dsids at once
	total = 0
	for dsid in dsids:
		# print("BEFORE total")
		# print(total)
		hist, bins = get_hist(args.path, dsid)
		total += hist
		# print("AFTER total")
		# print(total)
		# filename = "rnd_dijet_" + str(dsid)
		filename = "rnd_hbb_" + str(dsid)
		draw_hist(hist, bins, "/eos/user/o/okarkout/plots/", file_name=filename)
	#print("hists")
	#print(hists)
	histlist = []
	if dsids == []:
		total, bin_centers, errors= get_hist(args.path1, '')
		histlist.append([total, bin_centers, errors])
	
	total, bin_centers, errors = get_hist(args.path2, '')
	histlist.append([total, bin_centers, errors])


	
	print("done total")
	print(f"total {total}")
	print(f"bin_centers {bin_centers}")
	print(f"errors {errors}")
	draw_3_hist(histlist, 'pt', "/eos/user/o/okarkout/plots/", file_name="Dijets_vs_Higgs")
	# draw_hist(total, bin_centers, errors, "/eos/user/o/okarkout/plots/", file_name="rnd_reduced_dijet_unweighted_err")
	# draw_hist(total, bin_centers, errors, "/eos/user/o/okarkout/plots/", file_name="rnd_reduced_hbb_unweighted_err")
	# draw_hist(total, bin_centers, errors, "/eos/user/o/okarkout/plots/", file_name="rnd_top")

if __name__ == '__main__':
	run()


