import os, glob, h5py, json
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

def get_errors(bins, var, weights):
	errors = []
	bin_centers = []

	for bin_index in range(len(bins) - 1):

		# find which pt points are inside this bin
		bin_left = bins[bin_index]
		bin_right = bins[bin_index + 1]
		# in_bin = np.logical_and(bin_left < eta, eta <= bin_right)
		in_bin = np.logical_and(bin_left < var, var <= bin_right)
		# in_bin = np.logical_and(bin_left < DL1r, DL1r <= bin_right)

		# filter the weights to only those inside the bin
		weights_in_bin = weights[in_bin]

		# compute the error however you want
		error = np.sqrt(np.sum(weights_in_bin ** 2))
		errors.append(error)

		# save the center of the bins to plot the errorbar in the right place
		bin_center = (bin_right + bin_left) / 2
		bin_centers.append(bin_center)
	
	return bin_centers, errors

def get_hist(path):
	files=glob.glob(path)
	print(path)
	# samples = ["train"]
	samples = ["train", "valid", "test"]
	trainfile = h5py.File(path,"r")


	bin_edges_eta = np.arange(-2.5, 2.5, 0.1) #for eta
	bin_edges_DL1r = np.arange(-0.1, 1.1, 0.1) #for DL1r

	sam = np.vstack((trainfile.get('train'), trainfile.get('valid'), trainfile.get('test')))
	ws = None # sam[:,0]
	is_H = sam[:,37]

	pt_hist2d, _, bin_edges = np.histogram2d(is_H, sam[:,6], bins = [2,101], weights = ws)
	pt_bg_hist, pt_Hbb_hist = pt_hist2d[0], pt_hist2d[1]
	pt = [[pt_bg_hist.tolist(),bin_edges.tolist()], [pt_Hbb_hist.tolist(),bin_edges.tolist()]]

	pt_fine_hist2d, _, bin_edges_fine = np.histogram2d(is_H, sam[:,6], bins = [2,1001], weights = ws)
	pt_fine_bg_hist, pt_fine_Hbb_hist = pt_fine_hist2d[0], pt_fine_hist2d[1]
	pt_fine = [[pt_fine_bg_hist.tolist(),bin_edges_fine.tolist()], [pt_fine_Hbb_hist.tolist(),bin_edges_fine.tolist()]]

	eta_hist2d, _, _ = np.histogram2d(is_H, sam[:,7], bins = [2,bin_edges_eta], weights = ws)
	eta_bg_hist, eta_Hbb_hist = eta_hist2d[0], eta_hist2d[1]
	eta = [[eta_bg_hist.tolist(),bin_edges_eta.tolist()], [eta_Hbb_hist.tolist(),bin_edges_eta.tolist()]]

	DL1r1_hist2d, _, _ = np.histogram2d(is_H, sam[:,10], bins = [2,bin_edges_DL1r], weights = ws)
	DL1r1_bg_hist, DL1r1_Hbb_hist = DL1r1_hist2d[0], DL1r1_hist2d[1]
	DL1r1 = [[DL1r1_bg_hist.tolist(),bin_edges_DL1r.tolist()], [DL1r1_Hbb_hist.tolist(),bin_edges_DL1r.tolist()]]

	DL1r2_hist2d, _, _ = np.histogram2d(is_H, sam[:,8], bins = [2,bin_edges_DL1r], weights = ws)
	DL1r2_bg_hist, DL1r2_Hbb_hist = DL1r2_hist2d[0], DL1r2_hist2d[1]
	DL1r2 = [[DL1r2_bg_hist.tolist(),bin_edges_DL1r.tolist()], [DL1r2_Hbb_hist.tolist(),bin_edges_DL1r.tolist()]]

	return pt, pt_fine, eta, DL1r1, DL1r2

def draw_hist(histlist, xlabel, out_dir, file_name):
	i = 0
	colors = ['cyan', 'tab:blue', 'm', 'r']
	# colors = ['g', 'b', 'crimson']
	labels = ['Background_nan', 'Hbb_nonan', '_nonan']
	# labels = ['Dijets_gab_not_resampled', 'Hbb_gab_not_resampled', 'Dijets_nores', 'Hbb_nores']
	linestyles = ['solid', 'dashed', 'solid', 'dashed']
	transparency = [0.7, 0.7, 1, 1]
	linewidths = [1.5, 1.5, 1, 1]
	for item in histlist:
		print(i)
		if i < 5:
			hist, bins = item[0], item[1]
			# plt.errorbar(bin_centers, hist, yerr = errors, linewidth=linewidths[i], color = colors[i], label = labels[i], ecolor='r', capsize=2, marker = '', drawstyle = 'steps-mid')
			# plt.step(x=bins, y=hist, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
			plt.stairs(hist, edges=bins, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		i += 1
	plt.legend(loc="upper right", fontsize='x-small')
	plt.yscale('log')
	# plt.title(file_name, ha='right', y=0.98, fontsize=10)
	# plt.xlabel('eta', ha='right', x=0.98, fontsize=10)
	plt.xlabel(xlabel, ha='right', x=0.98, fontsize=10)
	# plt.xlabel('DL1r', ha='right', x=0.98, fontsize=10)
	plt.savefig(out_dir + "/hist_" + file_name + "_" + xlabel + ".pdf") #
	plt.close()

def run():
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('--path1',   dest='path1',   default='/eos/user/o/okarkout/Gab_Train_output/Gab_output_1/trainstd.h5')
	parser.add_argument('--path2',   dest='path2',   default='/eos/user/o/okarkout/Gab_Train_output/Gab_train/train.h5')
	parser.add_argument('--path3',   dest='path3',   default='/eos/user/o/okarkout/Gab_Train_output/Gab_comb/comb_trainstd.h5')
	parser.add_argument('--path4',   dest='path4',   default='../onestep/comb_unsampled_nonscaled.h5')
	parser.add_argument('--path5',   dest='path5',   default='../onestep/comb_res_10bin.h5')
	parser.add_argument('--path6',   dest='path6',   default='../onestep/comb_scale_res_2D_300bin.h5')
	parser.add_argument('--path7',   dest='path7',   default='../onestep/comb_noscale_nores_1000ptcut.h5')
	parser.add_argument('--path8',   dest='path8',   default='../onestep/comb_noscale_res_1000ptcut.h5')
	parser.add_argument('--path9',   dest='path9',   default='../onestep/test361029_comb_noscale_res_1000ptcut.h5')
	parser.add_argument('--path10',   dest='path10',   default='../onestep/test361029_comb_noscale_nores_1000ptcut.h5')
	parser.add_argument('--path11',   dest='path11',   default='../onestep/with361029_comb_noscale_nores_1000ptcut.h5')
	parser.add_argument('--path12',   dest='path12',   default='../onestep/with361029_comb_noscale_res_1000ptcut.h5')
	parser.add_argument('--path13',   dest='path13',   default='../onestep/with361029_comb_scale_res_1000ptcut.h5')
	parser.add_argument('--path14',   dest='path14',   default='../onestep/comb_gab_nonan_nores_noscale.h5')
	parser.add_argument('--path15',   dest='path15',   default='../onestep/comb_gab_nonan_res_noscale.h5')
	args = parser.parse_args()

	# save hists:
	# histlist = [get_hist(args.path12), get_hist(args.path15)]
	histlist = [get_hist(args.path11), get_hist(args.path14)]

	histdic = {'comb_res_noscale': histlist[0], 'nonan_nores_noscale': histlist[1]}
	a_file = open("histdic_H_vs_Di_nan_vs_nonan_noscale_res.json", "w")
	json.dump(histdic, a_file)
	a_file.close()

	# get hists from saved file:
	# a_file = open("histdic_H_vs_Di_noscale_gabtrain_vs_nores_2D300bin_1000ptcut.json", "r")
	# histdic_str = a_file.read()
	# a_file.close()
	# histdic = json.loads(histdic_str)
	# histlist = [histdic['comb_noscale_nores_1000ptcut'], histdic['comb_noscale_res_1000ptcut']]


	pt_not, pt_fine_not, eta_not, DL1r1_not, DL1r2_not = histlist[0]
	pt_re, pt_fine_re, eta_re, DL1r1_re, DL1r2_re = histlist[1]
	hist_pt = [pt_not[0], pt_not[1], pt_re[0], pt_re[1]]
	hist_pt_fine = [pt_fine_not[0], pt_fine_not[1], pt_fine_re[0], pt_fine_re[1]]
	hist_eta = [eta_not[0], eta_not[1], eta_re[0], eta_re[1]]
	hist_DL1r1 = [DL1r1_not[0], DL1r1_not[1], DL1r1_re[0], DL1r1_re[1]]
	hist_DL1r2 = [DL1r2_not[0], DL1r2_not[1], DL1r2_re[0], DL1r2_re[1]]
	
	draw_hist(hist_pt, 'pt', "/eos/user/o/okarkout/plots/", file_name="nantest_nores_noscale")
	draw_hist(hist_pt_fine, 'pt', "/eos/user/o/okarkout/plots/", file_name="nantest_nores_noscale_fine")
	draw_hist(hist_eta, 'eta', "/eos/user/o/okarkout/plots/", file_name="nantest_nores_noscale")
	draw_hist(hist_DL1r1, 'DL1r_pb_1', "/eos/user/o/okarkout/plots/", file_name="nantest_nores_noscale")
	draw_hist(hist_DL1r2, 'DL1r_pu_1', "/eos/user/o/okarkout/plots/", file_name="nantest_nores_noscale")

if __name__ == '__main__':
	run()


