import argparse
import os,glob,h5py,shutil
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages



print('loading models')
bbtagger=['/eos/user/o/okarkout/ResampledTraining/scaled_res_2D75bin/res_2D75bins_scaled_100Epochs_best.h5', '/eos/user/o/okarkout/ResampledTraining/nonan_res2D_75_100/nonan_res2D_75_100bins_scaled_100Epochs_best.h5']
train_files = ['/eos/user/o/okarkout/ResampledTraining/scaled_res_2D75bin/with361029_comb_scale_res_1000ptcut.h5', '/eos/user/o/okarkout/ResampledTraining/nonan_res2D_75_100/comb_gab_nonan_res_scale.h5']
ids = ['with_nan', 'without_nan']

modelPredictions=pd.DataFrame()
eff=[]
originalY=[]
colors = ['black', 'tab:blue', 'm']
for count in range(len(ids)):
    id = ids[count]
    model = tf.keras.models.load_model(bbtagger[count])
    
    print('Loading test data')
    train_file=h5py.File(train_files[count])
    test=train_file.get("test")
    fatjet =test[:, [ 6, 7]] # add JSS variables here AFTER 7 (and below in valid)
    subjet0=test[:, [ 8, 9,10]]
    subjet1=test[:, [11,12,13]]
    subjet2=test[:, [14,15,16]]
    test_y=test[:,[38]] #osama: why only [38]? because this is the answer the nn is supposed to guess.
    test_w=test[:,0] # use weight - should RW to flat distribution in pT


    print('testing model')
    # predictions={id:(model.predict(x=[fatjet,subjet0,subjet1,subjet2])[:,1])} #osama: why only [:,1]?
    # df=pd.DataFrame(predictions)
    # modelPredictions=pd.concat((modelPredictions,df), axis=1)
    modelPredictions = pd.DataFrame(model.predict(x=[fatjet,subjet0,subjet1,subjet2])[:,1])

    #calculate fpr and tpr of other taggers

    print('Calculating ' + id + ' ROC Curve')
    #ValueError: Found input variables with inconsistent numbers of samples: [1442474, 1493137, 1442474]
    # fpr, tpr, _ = roc_curve(test_y, modelPredictions[id], sample_weight=test_w)
    # fpr, tpr, _ = roc_curve(test_y, modelPredictions, sample_weight=test_w)
    fpr, tpr, _ = roc_curve(test_y, modelPredictions)
    info=np.column_stack((tpr,np.power(fpr,-1)))

    if count == 0:
        for i in range(100):
            j=i/100.0
            eff.append(j)
            a=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
            originalY.append(a)
    newY=[]
    for i in range(100):
        j=i/100.0
        b=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
        newY.append(b)
    #compute ratio of fpr to dxbb
    ratio=np.divide(newY,originalY)

    ax1=plt.subplot(2,1,1)
    if count == 0:
        plt.axvline(0.4, color='k', lw=0.8)
        plt.yscale("log", nonposy="clip")
        plt.xlim(0.1,1)
        plt.ylim(1, 10**4)
        ax1.minorticks_on()
        plt.grid(b=True, which='both', axis='x')
        plt.gca().set_xticklabels(['']*10)
        plt.ylabel('Multijet Rejection')

    plt.plot(tpr,np.power(fpr,-1), label=id, color=colors[count])#, ls=lines[id])
    print(id)

    ax2=plt.subplot(2,1,2)
    if count == 0:
        plt.axvline(0.4, color='k', lw=0.8)
        plt.xlim(0.1,1)
        plt.ylim(0,4)
        plt.xlabel('Higgs Efficiency')
        plt.ylabel('Ratio nonan/nan')
        ax2.minorticks_on()
        plt.grid(b=True, which='both', axis='x')
    plt.plot(eff, ratio, color=colors[count])#, ls=lines[id])


ax1.legend(loc='upper center', bbox_to_anchor=(0.49, 1.33), fancybox=True, shadow=True, ncol=2)
pp = PdfPages('MRcurve_compare_nan_noweights.pdf')
plt.subplots_adjust(wspace=0, hspace=0.1)
plt.savefig(pp, format='pdf')
pp.close()
print("Figures Saved to Comparison.pdf")

