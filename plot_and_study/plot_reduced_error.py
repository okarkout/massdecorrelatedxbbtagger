import glob
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

def get_hist(path, dsid):
	hist = 0
	files=glob.glob(path + "/*" + str(dsid) + "*/*.h5")
	if files == []:
		print('did not find files')
		exit()
	for fpath in files:
		print(fpath)

		store = pd.HDFStore(fpath)
		weights = store.get("dataset")["weight"]
		pt     = store.get("dataset")["pt"]
		store.close()

		bin_edges = np.arange(250, 2000, 10)
		histo, bins = np.histogram(pt, bins=bin_edges, weights=None)
		hist += histo

		errors = []
		bin_centers = []

		for bin_index in range(len(bins) - 1):

			# find which pt points are inside this bin
			bin_left = bins[bin_index]
			bin_right = bins[bin_index + 1]
			in_bin = np.logical_and(bin_left < pt, pt <= bin_right)

			# filter the weights to only those inside the bin
			weights_in_bin = weights[in_bin]

			# compute the error however you want
			error = np.sqrt(np.sum(weights_in_bin ** 2))
			errors.append(error)

			# save the center of the bins to plot the errorbar in the right place
			bin_center = (bin_right + bin_left) / 2
			bin_centers.append(bin_center)

		if np.any(np.isnan(histo)):
			print(fpath + " has NANs\n")
	# print(hist)
	return hist, bin_centers, errors

def draw_hist(hist, bin_centers, errors, out_dir, file_name):

	plt.errorbar(bin_centers, hist, yerr = errors, linewidth=1, ecolor='r', capsize=2, marker = '.', drawstyle = 'steps-mid', label = 'Hbb sample')
	plt.yscale('log')
	# plt.ylabel(file_name, ha='right', y=0.98, fontsize=10)
	plt.xlabel('pt in GeV', ha='right', x=0.98, fontsize=10)
	plt.legend(loc="upper right", fontsize='x-small')

	plt.savefig(out_dir + "/hist_" + file_name + "_pT.pdf")
	plt.close()

def run():
	parser = argparse.ArgumentParser(description=__doc__)
	# parser.add_argument('--path',   dest='path',   default='../../ReducedDijets/')
	parser.add_argument('--path',   dest='path',   default='../../ReducedHbb/')
	# parser.add_argument('--path',   dest='path',   default='../../ReducedTop/')
	args = parser.parse_args()

	# get a hist per dsid
	# dsids = [361020, 361021, 361022, 361023, 361024, 361025, 361026, 361027, 361028, 361029, 361030, 361031]
	# dsids = [361023, 361028]
	dsids = [] #run on all dsids at once
	total = 0
	for dsid in dsids:
		# print("BEFORE total")
		# print(total)
		hist, bins = get_hist(args.path, dsid)
		total += hist
		# print("AFTER total")
		# print(total)
		# filename = "rnd_dijet_" + str(dsid)
		filename = "rnd_hbb_" + str(dsid)
		draw_hist(hist, bins, "/eos/user/o/okarkout/plots/", file_name=filename)
	#print("hists")
	#print(hists)
	if dsids == []:
		total, bin_centers, errors= get_hist(args.path, '')

	print("done total")
	print(f"total {total}")
	print(f"bin_centers {bin_centers}")
	print(f"errors {errors}")

	# draw_hist(total, bin_centers, errors, "/eos/user/o/okarkout/plots/", file_name="rnd_reduced_dijet_unweighted_err")
	draw_hist(total, bin_centers, errors, "/eos/user/o/okarkout/plots/", file_name="gab_reduced_hbb_unweighted_err")
	# draw_hist(total, bin_centers, errors, "/eos/user/o/okarkout/plots/", file_name="rnd_top")

if __name__ == '__main__':
	run()


