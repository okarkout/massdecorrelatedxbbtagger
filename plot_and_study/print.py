import glob,math,os,h5py
import pandas as pd
import numpy as np

import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")

args = parser.parse_args()
path1=args.path

# filepathHiggs="../../FlattenData/MergedDijets.h5"
filePaths = glob.glob(args.path+"../DijetsDatasets/*.h5/*.h5")

for path in filePaths:
    print(path)
    hdf = pd.HDFStore(path, 'r')
    fj = hdf.get("fat_jet")['mcEventWeight']
    print(fj)


# hdf = pd.HDFStore(args.path)
# print(type(hdf))
# #print(hdf.keys())

# fj  = hdf.get("fat_jet")
# print(type(fj))
# print("fat jet")
# for c in fj.columns:
#     print("\t" + c)

# vr1 = hdf.get("subjet_VR_1")
# print("VR 1 jet")
# for c in vr1.columns:
#     print("\t" + c)

# f=h5py.File(path1, 'r')
# data=f.get("train")
#data=f.get("std")
#data=f.get('mean')
# print(data)
# var = data[:,16]
# print(min(var))
# weight = data[:,0]
# print(weight)
# pt = data[:,6]
# # print(pt)
# # print(pt.shape)
# # print(max(pt))
# for n in pt:
#     if pd.isna(n):
#         print('found')
#         print(np.nan_to_num(n))

# pt2 = np.append(np.array([]),pt)
# print(pt2.shape)

# pt2 = np.append(pt,pt2)
# print(pt2.shape)
