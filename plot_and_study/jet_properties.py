import os, glob, h5py, json
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

def cut(df, min_sj=1, ptCut=9200, relDrMin=1.0):
	# cut all events with pT>cut
	df.drop(df[df['pt'] >= ptCut].index, inplace=True)
	# cut on collinearity dR<1
	nRowOrig = len(df.index)

	print("Rows ratio after collinear cut: " + str(len(df.index) / nRowOrig))
	if min_sj==1:
		#drop fatjets with 0 sj. assumption: if subjet_1 is missing (nan), then all subjets are missing. you can check this assumption with /plot_and_study/jet_properties
		df.drop(df[np.isnan(df['relativeDeltaRToVRJet_1'])].index, inplace=True)
	elif min_sj==2:
		#drop fatjets with 1 or 0 sj. assumption: if there are at least 2 subjets, subjet_2 is always there (not nan).
		df.drop(df[np.isnan(df['relativeDeltaRToVRJet_2'])].index, inplace=True)
	elif min_sj==3:
		#drop fatjets with 2 or 1 or 0 sj. assumption: if there are 3 subjet, subjet_3 is always there (not nan).
		df.drop(df[np.isnan(df['relativeDeltaRToVRJet_3'])].index, inplace=True)
	elif min_sj==0:
		pass
	else:
		print('min_sj has to be 0, 1, 2, or 3')
		exit()

	print("Rows ratio after subjet cut: " + str(len(df.index) / nRowOrig))
	return df

def cutcol(df, max_col, relDrMin=1.0):
	if max_col==2: #drop fatjet if all subjets are collinear (missing subjet =nan is not collinear)
		return df[(df['relativeDeltaRToVRJet_1'] >= relDrMin) | (df['relativeDeltaRToVRJet_2'] >= relDrMin) | (df['relativeDeltaRToVRJet_3'] >= relDrMin)]
	elif max_col==1: #drop if 2 are col.
		return df[((df['relativeDeltaRToVRJet_1'] >= relDrMin) & (df['relativeDeltaRToVRJet_2'] >= relDrMin)) | ((df['relativeDeltaRToVRJet_3'] >= relDrMin) & (df['relativeDeltaRToVRJet_2'] >= relDrMin)) | ((df['relativeDeltaRToVRJet_1'] >= relDrMin) & (df['relativeDeltaRToVRJet_3'] >= relDrMin))]
	elif max_col==0: #drop if any collinear
		# df.drop(df[(df['relativeDeltaRToVRJet_1'] < relDrMin) | (df['relativeDeltaRToVRJet_2'] < relDrMin) | (df['relativeDeltaRToVRJet_3'] < relDrMin) ].index, inplace=True)
		return df[(df['relativeDeltaRToVRJet_1'] >= relDrMin) & (df['relativeDeltaRToVRJet_2'] >= relDrMin) & (df['relativeDeltaRToVRJet_3'] >= relDrMin)]
	elif max_col==3:
		return df
	else:
		print('max_col has to be 0, 1, 2, or 3')
		exit()

# def nosj(df):
# 	return df[np.isnan(df['relativeDeltaRToVRJet_1']) & np.isnan(df['relativeDeltaRToVRJet_2']) & np.isnan(df['relativeDeltaRToVRJet_3'])]

# def threesj(df):
# 	return df[~np.isnan(df['relativeDeltaRToVRJet_1']) & ~np.isnan(df['relativeDeltaRToVRJet_2']) & ~np.isnan(df['relativeDeltaRToVRJet_3'])]

# def onesj_ynn(df): #no yes yes
# 	return df[(~np.isnan(df['relativeDeltaRToVRJet_1']) & np.isnan(df['relativeDeltaRToVRJet_2']) & np.isnan(df['relativeDeltaRToVRJet_3']))]

# def onesj_nyn(df):
# 	return df[(np.isnan(df['relativeDeltaRToVRJet_1']) & ~np.isnan(df['relativeDeltaRToVRJet_2']) & np.isnan(df['relativeDeltaRToVRJet_3']))]

# def onesj_nny(df):
# 	return df[(np.isnan(df['relativeDeltaRToVRJet_1']) & np.isnan(df['relativeDeltaRToVRJet_2']) & ~np.isnan(df['relativeDeltaRToVRJet_3']))]

# def twosj_yyn(df):
# 	return df[(~np.isnan(df['relativeDeltaRToVRJet_1']) & ~np.isnan(df['relativeDeltaRToVRJet_2']) & np.isnan(df['relativeDeltaRToVRJet_3']))]

# def twosj_yny(df):
# 	return df[(~np.isnan(df['relativeDeltaRToVRJet_1']) & np.isnan(df['relativeDeltaRToVRJet_2']) & ~np.isnan(df['relativeDeltaRToVRJet_3']))]

# def twosj_nyy(df):
# 	return df[(np.isnan(df['relativeDeltaRToVRJet_1']) & ~np.isnan(df['relativeDeltaRToVRJet_2']) & ~np.isnan(df['relativeDeltaRToVRJet_3']))]

def flatten(path, edges):
	'''
	path: an h5 file path in a directory for the given process and dsid
	returns data to be appended to the output file
	
	edits: make features and cuts global variables.
	'''
	# need to add eventNumber back
	#features_group1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','eventNumber','dsid','mass','pt','eta'] #0-9
	features_group1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','dsid','mass','pt','eta'] #0-9
	#features_group2=['DL1rT_pu_1', 'DL1rT_pc_1', 'DL1rT_pb_1','DL1rT_pu_2', 'DL1rT_pc_2', 'DL1rT_pb_2','DL1rT_pu_3', 'DL1rT_pc_3', 'DL1rT_pb_3']
	features_group2=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
	#features_group3=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #9-27
	features_group4=['Split12', 'Split23', 'Qw', 'PlanarFlow', 'Angularity', 'Aplanarity', 'ZCut12', 'KtDR', 'C2', 'D2', 'e3', 'Tau21_wta', 'Tau32_wta', 'FoxWolfram20'] #27-41
	features_group5=['JetFitter_N2Tpair_1', 'JetFitter_dRFlightDir_1', 'JetFitter_deltaeta_1', 'JetFitter_deltaphi_1', 'JetFitter_energyFraction_1', 'JetFitter_mass_1', 'JetFitter_massUncorr_1', 'JetFitter_nSingleTracks_1', 'JetFitter_nTracksAtVtx_1', 'JetFitter_nVTX_1', 'JetFitter_significance3d_1','SV1_L3d_1', 'SV1_Lxy_1', 'SV1_N2Tpair_1', 'SV1_NGTinSvx_1', 'SV1_deltaR_1', 'SV1_dstToMatLay_1', 'SV1_efracsvx_1', 'SV1_masssvx_1', 'SV1_significance3d_1', 'rnnip_pu_1', 'rnnip_pc_1', 'rnnip_pb_1', 'rnnip_ptau_1', 'JetFitter_N2Tpair_2', 'JetFitter_dRFlightDir_2', 'JetFitter_deltaeta_2', 'JetFitter_deltaphi_2', 'JetFitter_energyFraction_2', 'JetFitter_mass_2', 'JetFitter_massUncorr_2', 'JetFitter_nSingleTracks_2', 'JetFitter_nTracksAtVtx_2', 'JetFitter_nVTX_2', 'JetFitter_significance3d_2', 'SV1_L3d_2', 'SV1_Lxy_2', 'SV1_N2Tpair_2', 'SV1_NGTinSvx_2', 'SV1_deltaR_2', 'SV1_dstToMatLay_2', 'SV1_efracsvx_2', 'SV1_masssvx_2', 'SV1_significance3d_2',  'rnnip_pu_2', 'rnnip_pc_2', 'rnnip_pb_2', 'rnnip_ptau_2', 'JetFitter_N2Tpair_3', 'JetFitter_dRFlightDir_3', 'JetFitter_deltaeta_3', 'JetFitter_deltaphi_3', 'JetFitter_energyFraction_3', 'JetFitter_mass_3', 'JetFitter_massUncorr_3', 'JetFitter_nSingleTracks_3', 'JetFitter_nTracksAtVtx_3', 'JetFitter_nVTX_3', 'JetFitter_significance3d_3', 'SV1_L3d_3', 'SV1_Lxy_3', 'SV1_N2Tpair_3', 'SV1_NGTinSvx_3', 'SV1_deltaR_3', 'SV1_dstToMatLay_3', 'SV1_efracsvx_3', 'SV1_masssvx_3', 'SV1_significance3d_3', 'rnnip_pu_3', 'rnnip_pc_3', 'rnnip_pb_3', 'rnnip_ptau_3'] #41-113

	features=features_group1+features_group2+features_group4

	#df = pd.read_hdf(path, columns=features) #replaced with HDFStore to be able to close the file within the code before opening another.
	store = pd.HDFStore(path)
	df = store.get("dataset")[features] #runs on files from Reduced*
	store.close()
	df = cut(df)
	three = cutcol(df, max_col=3)
	two = cutcol(df, max_col=2)
	one = cutcol(df, max_col=1)
	non = cutcol(df, max_col=0)

	print(len(non.index))
	print(len(one.index))
	if len(non.index) == len(two.index):
		exit()
	print(len(two.index))
	print(len(three.index))
	print(len(df.index))

	hist0, _ = np.histogram(non['pt'], bins = edges)
	hist1, _ = np.histogram(one['pt'], bins = edges)
	hist2, _ = np.histogram(two['pt'], bins = edges)
	hist3, _ = np.histogram(three['pt'], bins = edges)

	return np.array([hist0, hist1, hist2, hist3])

def draw_hist(histlist, edges, xlabel, out_dir, file_name):
	i = 0
	# colors = ['black', 'navy', 'b', 'tab:blue', 'brown', 'r', 'm', 'green']
	# labels = ['0sj', 'ynn', 'nyn', 'nny', 'nyy', 'yny', 'yyn', '3sj']
	colors = ['black', 'navy', 'm', 'green']
	labels = ['max_col=0', 'max_col=1', 'max_col=2', 'max_col=3']
	linestyles = ['solid', 'solid', 'solid', 'solid', 'solid', 'solid', 'solid', 'solid']
	transparency = [1, 1, 1, 1, 1, 1, 1, 1]
	linewidths = [1, 1, 1, 1, 1, 1, 1, 1]
	for hist in histlist:
		print('total number of jets for histogram:')
		print(i)
		print('is')
		print(np.sum(hist))
		# plt.errorbar(bin_centers, hist, yerr = errors, linewidth=linewidths[i], color = colors[i], label = labels[i], ecolor='r', capsize=2, marker = '', drawstyle = 'steps-mid')
		# plt.step(x=bins, y=hist, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		plt.stairs(hist, edges=edges, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		i += 1
	plt.legend(loc="upper right", fontsize='x-small')
	plt.yscale('log')
	# plt.title(file_name, ha='right', y=0.98, fontsize=10)
	# plt.xlabel('eta', ha='right', x=0.98, fontsize=10)
	plt.xlabel(xlabel, ha='right', x=0.98, fontsize=10)
	# plt.xlabel('DL1r', ha='right', x=0.98, fontsize=10)
	plt.savefig(out_dir + "/hist_" + file_name + "_" + xlabel + ".pdf") #
	plt.close()

paths = sorted(glob.glob("/eos/user/o/okarkout/full_run_v3/copies/ReducedDijets/*/*.h5"))

j=0
edges = np.linspace(200, 9200, 901)
for Path in paths:
	j+=1
	print('handling file number ' + str(j))
	print(Path)
	if j == 1:
		histos = flatten(Path, edges)
		print('first check')
		print(len(histos))
	else:
		h = flatten(Path, edges)
		histos += h

hists = histos.tolist()
print(len(hists))
print('all jets:')
print(np.sum(histos))

# print('jets with 0 subjets:')
# print(np.sum(hists[0]))
# print('jets with 1 subjets ynn:')
# print(np.sum(hists[1]))
# print('jets with 1 subjets nyn:')
# print(np.sum(hists[2]))
# print('jets with 1 subjets nny:')
# print(np.sum(hists[3]))
# print('jets with 2 subjets nyy:')
# print(np.sum(hists[4]))
# print('jets with 2 subjets yny:')
# print(np.sum(hists[5]))
# print('jets with 2 subjets yyn:')
# print(np.sum(hists[6]))
# print('jets with 3 subjets:')
# print(np.sum(hists[7]))

histdic = {'Dijetsjets_properties_hists_max_col': hists}
a_file = open("Dijetsjets_properties_hists_max_col.json", "w")
json.dump(histdic, a_file)
a_file.close()


# # get hists from saved file:
# a_file = open("jets_properties_hists.json", "r")
# histdic_str = a_file.read()
# a_file.close()
# histdic = json.loads(histdic_str)
# hists = histdic['jets_properties_hists']
# _, edges = flatten(paths[0])

draw_hist(hists, edges, 'Dijets_pt (GeV)', '/eos/user/o/okarkout/plots/', file_name = 'Dijets_max_col')