import os, glob, h5py, json
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

"""
for when there's no top
"""

def get_hist(path):
	files=glob.glob(path)
	print(path)
	# samples = ["train"]
	samples = ["train", "valid", "test"]
	trainfile = h5py.File(path,"r")

	bin_edges_eta = np.arange(-2.5, 2.5, 0.1) #for eta
	bin_edges_DL1r = np.arange(-0.1, 1.1, 0.01) #for DL1r

	sam = np.vstack((trainfile.get('train'), trainfile.get('valid'), trainfile.get('test')))
	ws = None # sam[:,0]
	is_di = sam[:,38]

	pt_hist2d, _, bin_edges = np.histogram2d(is_di, sam[:,6], bins = [2,101], weights = ws)
	pt_dijets_hist, pt_Hbb_hist = pt_hist2d[0], pt_hist2d[1]
	pt = [[pt_dijets_hist.tolist(),bin_edges.tolist()], [pt_Hbb_hist.tolist(),bin_edges.tolist()]]

	# pt_fine_hist2d, _, bin_edges_fine = np.histogram2d(is_di, sam[:,6], bins = [2,1001], weights = ws)
	# pt_fine_dijets_hist, pt_fine_Hbb_hist = pt_fine_hist2d[0], pt_fine_hist2d[1]
	# pt_fine = [[pt_fine_dijets_hist.tolist(),bin_edges_fine.tolist()], [pt_fine_Hbb_hist.tolist(),bin_edges_fine.tolist()]]

	eta_hist2d, _, _ = np.histogram2d(is_di, sam[:,7], bins = [2,bin_edges_eta], weights = ws)
	eta_dijets_hist, eta_Hbb_hist = eta_hist2d[0], eta_hist2d[1]
	eta = [[eta_dijets_hist.tolist(),bin_edges_eta.tolist()], [eta_Hbb_hist.tolist(),bin_edges_eta.tolist()]]

	Dlist = []
	for i in range(8,17):
		DL1r_hist2d, _, _ = np.histogram2d(is_di, sam[:,i], bins = [2,bin_edges_DL1r], weights = ws)
		DL1r_dijets_hist, DL1r_Hbb_hist = DL1r_hist2d[0], DL1r_hist2d[1]
		DL1r = [[DL1r_dijets_hist.tolist(),bin_edges_DL1r.tolist()], [DL1r_Hbb_hist.tolist(),bin_edges_DL1r.tolist()]]
		Dlist.append(DL1r)

	return pt, eta, Dlist

def draw_hist(histlist, xlabel, out_dir, file_name):
	i = 0
	colors = ['cyan', 'tab:blue', 'm', 'r']
	# colors = ['g', 'b', 'crimson']
	labels = ['Dijets_nan', 'Hbb_nan', 'Dijets_nonan', 'Hbb_nonan']
	# labels = ['Dijets_gab_not_resampled', 'Hbb_gab_not_resampled', 'Dijets_nores', 'Hbb_nores']
	linestyles = ['solid', 'dashed', 'solid', 'dashed']
	transparency = [0.7, 0.7, 1, 1]
	linewidths = [1.5, 1.5, 1, 1]
	for item in histlist:
		print(i)
		if i < 5:
			hist, bins = item[0], item[1]
			# plt.errorbar(bin_centers, hist, yerr = errors, linewidth=linewidths[i], color = colors[i], label = labels[i], ecolor='r', capsize=2, marker = '', drawstyle = 'steps-mid')
			# plt.step(x=bins, y=hist, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
			plt.stairs(hist, edges=bins, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		i += 1
	plt.legend(loc="upper right", fontsize='x-small')
	plt.yscale('log')
	# plt.title(file_name, ha='right', y=0.98, fontsize=10)
	# plt.xlabel('eta', ha='right', x=0.98, fontsize=10)
	plt.xlabel(xlabel, ha='right', x=0.98, fontsize=10)
	# plt.xlabel('DL1r', ha='right', x=0.98, fontsize=10)
	plt.savefig(out_dir + "/hist_" + file_name + "_fine_" + xlabel + ".pdf") #
	plt.close()

def run():
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('--path1',   dest='path1',   default='/eos/user/o/okarkout/Gab_Train_output/Gab_output_1/trainstd.h5')
	parser.add_argument('--path2',   dest='path2',   default='/eos/user/o/okarkout/Gab_Train_output/Gab_train/train.h5')
	parser.add_argument('--path3',   dest='path3',   default='/eos/user/o/okarkout/Gab_Train_output/Gab_comb/comb_trainstd.h5')
	parser.add_argument('--path4',   dest='path4',   default='../onestep/comb_unsampled_nonscaled.h5')
	parser.add_argument('--path5',   dest='path5',   default='../onestep/comb_res_10bin.h5')
	parser.add_argument('--path6',   dest='path6',   default='../onestep/comb_scale_res_2D_300bin.h5')
	parser.add_argument('--path7',   dest='path7',   default='../onestep/comb_noscale_nores_1000ptcut.h5')
	parser.add_argument('--path8',   dest='path8',   default='../onestep/comb_noscale_res_1000ptcut.h5')
	parser.add_argument('--path9',   dest='path9',   default='../onestep/test361029_comb_noscale_res_1000ptcut.h5')
	parser.add_argument('--path10',   dest='path10',   default='../onestep/test361029_comb_noscale_nores_1000ptcut.h5')
	parser.add_argument('--path11',   dest='path11',   default='../onestep/with361029_comb_noscale_nores_1000ptcut.h5')
	parser.add_argument('--path12',   dest='path12',   default='../onestep/with361029_comb_noscale_res_1000ptcut.h5')
	parser.add_argument('--path13',   dest='path13',   default='../onestep/with361029_comb_scale_res_1000ptcut.h5')
	parser.add_argument('--path14',   dest='path14',   default='../onestep/comb_gab_nonan_nores_noscale.h5')
	parser.add_argument('--path15',   dest='path15',   default='../onestep/comb_gab_nonan_res_noscale.h5')
	args = parser.parse_args()

	# save hists:
	# histlist = [get_hist(args.path12), get_hist(args.path15)]
	histlist = [get_hist(args.path11), get_hist(args.path14)]

	histdic = {'comb_nores_noscale': histlist[0], 'nonan_nores_noscale': histlist[1]}
	a_file = open("histdic_H_vs_Di_nan_vs_nonan_fine_DL1r.json", "w")
	json.dump(histdic, a_file)
	a_file.close()

	# # get hists from saved file:
	# a_file = open("histdic_H_vs_Di_nan_vs_nonan_DL1r.json", "r")
	# histdic_str = a_file.read()
	# a_file.close()
	# histdic = json.loads(histdic_str)
	# histlist = [histdic['comb_nores_noscale'], histdic['nonan_nores_noscale']]

	pt_not, eta_not, DL1rlist_not = histlist[0]
	pt_re, eta_re, DL1rlist_re = histlist[1]
	hist_pt = [pt_not[0], pt_not[1], pt_re[0], pt_re[1]]
	hist_eta = [eta_not[0], eta_not[1], eta_re[0], eta_re[1]]
	dl1rlabels = ['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1', 'DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2', 'DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3']
	for i in range(0,9):
		hist_DL1r = [DL1rlist_not[i][0], DL1rlist_not[i][1], DL1rlist_re[i][0], DL1rlist_re[i][1]]
		draw_hist(hist_DL1r, dl1rlabels[i], "/eos/user/o/okarkout/plots/", file_name="nonan_nores_noscale")

	# draw_hist(hist_pt, 'pt', "/eos/user/o/okarkout/plots/", file_name="nantest_nores_noscale")
	# draw_hist(hist_eta, 'eta', "/eos/user/o/okarkout/plots/", file_name="nantest_nores_noscale")

if __name__ == '__main__':
	run()


