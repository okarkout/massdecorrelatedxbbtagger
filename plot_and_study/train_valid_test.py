import os, glob, h5py, json
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

def get_hist(path):
	files=glob.glob(path)
	print(path)
	samples = ["train", "valid", "test"]
	trainfile = h5py.File(path,"r")

	outlist = [0,0,0]

	# bin_edges = np.arange(-2.5, 2.6, 0.1) #for eta
	if 'std' in path:
		bin_edges = np.arange(-2, 12, 0.1) #for DL1r scaled
	else:
		bin_edges = np.arange(-1, 3, 0.1) #for DL1r unscaled
	
	for i in [0,1,2]:
		sam = trainfile.get(samples[i])
		var    = np.nan_to_num(sam[:, 16]) #eta is 7, DL1r_pb_3 is 16
		weights= sam[:, 0]

		hist, bins = np.histogram(var, bins=bin_edges, weights=weights)

		errors = []
		bin_centers = []
		
		for bin_index in range(len(bins) - 1):

			# find which pt points are inside this bin
			bin_left = bins[bin_index]
			bin_right = bins[bin_index + 1]
			in_bin = np.logical_and(bin_left < var, var <= bin_right)
			# in_bin = np.logical_and(bin_left < pt, pt <= bin_right)
			# in_bin = np.logical_and(bin_left < DL1r_pb_1, DL1r_pb_1 <= bin_right)

			# filter the weights to only those inside the bin
			weights_in_bin = weights[in_bin]

			# compute the error however you want
			error = np.sqrt(np.sum(weights_in_bin ** 2))
			errors.append(error)

			# save the center of the bins to plot the errorbar in the right place
			bin_center = (bin_right + bin_left) / 2
			bin_centers.append(bin_center)

		if np.any(np.isnan(hist)):
			print(path + " has NANs\n")
		
		outlist[i] = [hist.tolist(), bin_centers, errors]

	# print(hist)
	return outlist

def draw_hist(histlist, out_dir, file_name):
	i = 0
	colors = ['b', 'y', 'darkgreen']
	linewidths = [1.3, 1.1, 0.9]
	labels = ["train", "valid", "test"]
	for item in histlist:
		print(i)
		hist, bin_centers, errors = item[0], item[1], item[2]
		plt.errorbar(bin_centers, hist, yerr = errors, linewidth=linewidths[i], color = colors[i], label = labels[i], ecolor='r', capsize=2, marker = '', drawstyle = 'steps-mid')
		i += 1
	plt.legend(loc="upper left")
	plt.yscale('log')
	plt.ylabel(file_name, ha='right', y=0.98, fontsize=10)
	plt.xlabel('DL1r_pb_3', ha='right', x=0.98, fontsize=10)
	# plt.xlabel('pt in GeV', ha='right', x=0.98, fontsize=10)
	# plt.xlabel('DL1r_pb_1', ha='right', x=0.98, fontsize=10)

	plt.savefig(out_dir + "/hist_" + file_name + "_DL1r_pb_3.pdf") #

def run():
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('--path1',   dest='path1',   default='../train/trainstd.h5')
	parser.add_argument('--path2',   dest='path2',   default='../train/train.h5')
	parser.add_argument('--path3',   dest='path3',   default='../train/comb_trainstd.h5')
	parser.add_argument('--path4',   dest='path4',   default='../train/comb_train.h5')
	args = parser.parse_args()

	#save hists:
	histlist = [get_hist(args.path1), get_hist(args.path2), get_hist(args.path3), get_hist(args.path4)]
	histdic = {'trainstd_DL1r_pb_3': histlist[0], 'train_DL1r_pb_3': histlist[1], 'comb_trainstd_DL1r_pb_3': histlist[2], 'comb_train_DL1r_pb_3': histlist[3]}
	a_file = open("train_valid_test_DL1r_pb_3.json", "w")
	json.dump(histdic, a_file)
	a_file.close()

	# # get hists from saved file:
	# a_file = open("train_valid_test_DL1r_pb_3.json", "r")
	# histdic_str = a_file.read()
	# a_file.close()
	# histdic = json.loads(histdic_str)


	histlist = histdic['comb_trainstd_DL1r_pb_3']
	draw_hist(histlist, "/eos/user/o/okarkout/plots/", file_name="restructured_scaled")

if __name__ == '__main__':
	run()


