import os, glob, h5py, json
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

def get_errors(bins, var, weights):
	errors = []
	bin_centers = []

	for bin_index in range(len(bins) - 1):

		# find which pt points are inside this bin
		bin_left = bins[bin_index]
		bin_right = bins[bin_index + 1]
		# in_bin = np.logical_and(bin_left < eta, eta <= bin_right)
		in_bin = np.logical_and(bin_left < var, var <= bin_right)
		# in_bin = np.logical_and(bin_left < DL1r, DL1r <= bin_right)

		# filter the weights to only those inside the bin
		weights_in_bin = weights[in_bin]

		# compute the error however you want
		error = np.sqrt(np.sum(weights_in_bin ** 2))
		errors.append(error)

		# save the center of the bins to plot the errorbar in the right place
		bin_center = (bin_right + bin_left) / 2
		bin_centers.append(bin_center)
	
	return bin_centers, errors

def get_3_hists(path):
	files=glob.glob(path)
	print(path)
	# samples = ["train"]
	samples = ["train", "valid", "test"]
	trainfile = h5py.File(path,"r")

	sam = np.vstack((trainfile.get('train'), trainfile.get('valid'), trainfile.get('test')))
	ws = None # sam[:,0]
	proc = sam[:,38] + 2 * sam[:,39] # 0 for Di, 1 for H, 2 for Top

	pt_hist3d, _, bin_edges = np.histogram2d(proc, sam[:,6], bins = [3,101], weights = ws)
	pt_Di_hist, pt_H_hist, pt_Top_hist = pt_hist3d[0], pt_hist3d[1], pt_hist3d[2]
	pt = [[pt_Di_hist.tolist(),bin_edges.tolist()], [pt_H_hist.tolist(),bin_edges.tolist()], [pt_Top_hist.tolist(),bin_edges.tolist()]]

	# pt_fine_hist3d, _, bin_edges_fine = np.histogram2d(proc, sam[:,6], bins = [3,1001], weights = ws)
	# pt_fine_Di_hist, pt_fine_H_hist, pt_fine_Top_hist = pt_fine_hist3d[0], pt_fine_hist3d[1], pt_fine_hist3d[2]
	# pt_fine = [[pt_fine_Di_hist.tolist(),bin_edges_fine.tolist()], [pt_fine_H_hist.tolist(),bin_edges_fine.tolist()], [pt_fine_Top_hist.tolist(),bin_edges_fine.tolist()]]

	eta_hist3d, _, bin_edges_eta = np.histogram2d(proc, sam[:,7], bins = [3,101], weights = ws)
	eta_Di_hist, eta_H_hist, eta_Top_hist = eta_hist3d[0], eta_hist3d[1], eta_hist3d[2]
	eta = [[eta_Di_hist.tolist(),bin_edges_eta.tolist()], [eta_H_hist.tolist(),bin_edges_eta.tolist()], [eta_Top_hist.tolist(),bin_edges_eta.tolist()]]

	Dlist = []
	for i in range(8,17):
		DL1r_hist3d, _, bin_edges_DL1r = np.histogram2d(proc, sam[:,i], bins = [3,101], weights = ws)
		DL1r_Di_hist, DL1r_H_hist , DL1r_Top_hist = DL1r_hist3d[0], DL1r_hist3d[1], DL1r_hist3d[2]
		DL1r = [[DL1r_Di_hist.tolist(),bin_edges_DL1r.tolist()], [DL1r_H_hist.tolist(),bin_edges_DL1r.tolist()], [DL1r_Top_hist.tolist(),bin_edges_DL1r.tolist()]]
		Dlist.append(DL1r)

	return pt, eta, Dlist

def get_2_hist(path):
	'''for when there's no top'''
	files=glob.glob(path)
	print(path)
	# samples = ["train"]
	samples = ["train", "valid", "test"]
	trainfile = h5py.File(path,"r")

	# bin_edges_eta = np.arange(-2.5, 2.5, 0.1) #for eta
	# bin_edges_DL1r = np.arange(-0.1, 1.1, 0.01) #for DL1r

	sam = np.vstack((trainfile.get('train'), trainfile.get('valid'), trainfile.get('test')))
	ws = None # sam[:,0]
	is_H = sam[:,38]

	pt_hist2d, _, bin_edges = np.histogram2d(is_H, sam[:,6], bins = [2,101], weights = ws)
	pt_dijets_hist, pt_Hbb_hist = pt_hist2d[0], pt_hist2d[1]
	pt = [[pt_dijets_hist.tolist(),bin_edges.tolist()], [pt_Hbb_hist.tolist(),bin_edges.tolist()]]

	# pt_fine_hist2d, _, bin_edges_fine = np.histogram2d(is_H, sam[:,6], bins = [2,1001], weights = ws)
	# pt_fine_dijets_hist, pt_fine_Hbb_hist = pt_fine_hist2d[0], pt_fine_hist2d[1]
	# pt_fine = [[pt_fine_dijets_hist.tolist(),bin_edges_fine.tolist()], [pt_fine_Hbb_hist.tolist(),bin_edges_fine.tolist()]]

	eta_hist2d, _, _ = np.histogram2d(is_H, sam[:,7], bins = [2,101], weights = ws)
	eta_dijets_hist, eta_Hbb_hist = eta_hist2d[0], eta_hist2d[1]
	eta = [[eta_dijets_hist.tolist(),bin_edges_eta.tolist()], [eta_Hbb_hist.tolist(),bin_edges_eta.tolist()]]

	Dlist = []
	for i in range(8,17):
		DL1r_hist2d, _, _ = np.histogram2d(is_H, sam[:,i], bins = [2,101], weights = ws)
		DL1r_dijets_hist, DL1r_Hbb_hist = DL1r_hist2d[0], DL1r_hist2d[1]
		DL1r = [[DL1r_dijets_hist.tolist(),bin_edges_DL1r.tolist()], [DL1r_Hbb_hist.tolist(),bin_edges_DL1r.tolist()]]
		Dlist.append(DL1r)

	return pt, eta, Dlist

def draw_hist(histlist, xlabel, out_dir, file_name):
	i = 0
	colors = ['tab:blue', 'm']
	# colors = ['g', 'b', 'crimson']
	labels = ['Dijets', 'Hbb']
	# labels = ['Dijets_gab_not_resampled', 'Hbb_gab_not_resampled', 'Dijets_nores', 'Hbb_nores']
	linestyles = ['solid', 'dashed']
	transparency = [0.7, 1]
	linewidths = [1.5, 1]
	for item in histlist:
		print(i)
		if i < 5:
			hist, bins = item[0], item[1]
			# plt.errorbar(bin_centers, hist, yerr = errors, linewidth=linewidths[i], color = colors[i], label = labels[i], ecolor='r', capsize=2, marker = '', drawstyle = 'steps-mid')
			# plt.step(x=bins, y=hist, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
			plt.stairs(hist, edges=bins, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		i += 1
	plt.legend(loc="upper right", fontsize='x-small')
	plt.yscale('log')
	# plt.title(file_name, ha='right', y=0.98, fontsize=10)
	# plt.xlabel('eta', ha='right', x=0.98, fontsize=10)
	plt.xlabel(xlabel, ha='right', x=0.98, fontsize=10)
	# plt.xlabel('DL1r', ha='right', x=0.98, fontsize=10)
	plt.savefig(out_dir + "/hist_" + file_name + "_" + xlabel + ".pdf") #
	plt.close()

def draw_3_hist(histlist, xlabel, out_dir, file_name):
	i = 0
	colors = ['m', 'tab:blue', 'g']
	# colors = ['g', 'b', 'crimson']
	labels = ['DiJets', 'Hbb', 'Top']
	linestyles = ['dotted', 'dashed', 'dotted']
	transparency = [0.7, 0.7, 0.7]
	linewidths = [1.5, 1.5, 1.5]
	for item in histlist:
		print(i)
		if i < 5:
			hist, bins = item[0], item[1]
			# plt.errorbar(bin_centers, hist, yerr = errors, linewidth=linewidths[i], color = colors[i], label = labels[i], ecolor='r', capsize=2, marker = '', drawstyle = 'steps-mid')
			# plt.step(x=bins, y=hist, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
			plt.stairs(hist, edges=bins, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		i += 1
	plt.legend(loc="upper right", fontsize='x-small')
	plt.yscale('log')
	# plt.title(file_name, ha='right', y=0.98, fontsize=10)
	# plt.xlabel('eta', ha='right', x=0.98, fontsize=10)
	plt.xlabel(xlabel, ha='right', x=0.98, fontsize=10)
	# plt.xlabel('DL1r', ha='right', x=0.98, fontsize=10)
	plt.savefig(out_dir + "/hist_" + file_name + "_" + xlabel + ".pdf") #
	plt.close()

def run():
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('--path1',   dest='path1',   default='../../Onestep/full_trainstd_newcuts.h5')
	# parser.add_argument('--path1',   dest='path1',   default='../../Onestep/full_trainstd.h5')
	args = parser.parse_args()

	# save hists:
	# histlist = [get_hist(args.path12), get_hist(args.path15)]
	histlist = [get_3_hists(args.path1)]

	histdic = {'full_default': histlist[0]}
	a_file = open("three_hists_full_default.json", "w")
	json.dump(histdic, a_file)
	a_file.close()

	# get hists from saved file:
	# a_file = open("histdic_H_vs_Di_noscale_gabtrain_vs_nores_2D300bin_1000ptcut.json", "r")
	# histdic_str = a_file.read()
	# a_file.close()
	# histdic = json.loads(histdic_str)
	# histlist = [histdic['comb_noscale_nores_1000ptcut'], histdic['comb_noscale_res_1000ptcut']]


	pt, eta, Dlist = histlist[0]
	hist_pt = [pt[0], pt[1], pt[2]]
	# hist_pt_fine = [pt_fine[0], pt_fine[1], pt_fine[2]]
	hist_eta = [eta[0], eta[1], eta[2]]
	dl1rlabels = ['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1', 'DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2', 'DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3']
	for i in range(0,9):
		hist_DL1r = [Dlist[i][0], Dlist[i][1], Dlist[i][2]]
		draw_3_hist(hist_DL1r, dl1rlabels[i], "/eos/user/o/okarkout/plots/", file_name="full_default_newcuts_H_Di_Top")

	draw_3_hist(hist_pt, 'pt', "/eos/user/o/okarkout/plots/", file_name="full_default_newcuts_H_Di_Top")
	# draw_3_hist(hist_pt_fine, 'pt', "/eos/user/o/okarkout/plots/", file_name="full_default_newcuts_H_Di_Top_fine")
	draw_3_hist(hist_eta, 'eta', "/eos/user/o/okarkout/plots/", file_name="full_default_newcuts_H_Di_Top")
	
if __name__ == '__main__':
	run()


