import glob, json
import argparse
import numpy as np
import pandas as pd
# from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot as plt

def get_hist(path, bin_edges, features):
	hist = 0
	dsid = ''
	files=glob.glob(path + "/*" + str(dsid) + "*/*.h5")
	if files == []:
		print('did not find files')
		exit()
	
	hist9 = np.zeros((9,len(bin_edges)-1))
	for fpath in files:
		print(fpath)
		filehist = []
		store = pd.HDFStore(fpath)
		for DL in features:
			DL1r = store.get("dataset")[DL]
			histo, _ = np.histogram(DL1r, bins=bin_edges, weights=None)
			if np.any(np.isnan(histo)):
				print(fpath + " has NANs\n")
			filehist.append(histo)
		hist9 += np.array(filehist)
	store.close()
	# print(hist)
	return hist9


def draw_hist(histlist, bins, xlabel, out_dir, file_name):
	i = 0
	colors = ['tab:blue', 'r']
	labels = ['Dijets_input', 'Hbb_input']
	linestyles = ['solid', 'solid']
	transparency = [0.7, 0.7]
	linewidths = [1, 1]

	for hist in histlist:
		print(i)
		plt.stairs(hist, edges=bins, linewidth=linewidths[i], color = colors[i], label = labels[i], linestyle = linestyles[i])
		i += 1
	plt.legend(loc="upper right", fontsize='x-small')
	plt.yscale('log')


	plt.xlabel(xlabel, ha='right', x=0.98, fontsize=10)

	plt.savefig(out_dir + "/hist_" + file_name + "_fine_" + xlabel + ".pdf") #
	plt.close()

def run():
	parser = argparse.ArgumentParser(description=__doc__)
	parser.add_argument('--path1',   dest='path1',   default='../../ReducedDijets/')
	parser.add_argument('--path2',   dest='path2',   default='../../ReducedHbb/')
	# parser.add_argument('--path',   dest='path',   default='../../ReducedTop/')
	args = parser.parse_args()

	features=['DL1r_pu_1', 'DL1r_pc_1', 'DL1r_pb_1','DL1r_pu_2', 'DL1r_pc_2', 'DL1r_pb_2','DL1r_pu_3', 'DL1r_pc_3', 'DL1r_pb_3'] #8-16
	bins = np.arange(-0.1, 1.1, 0.01) #for DL1r
	histlist = [get_hist(args.path1, bins, features), get_hist(args.path2, bins, features)]

	histdic = {'ReducedDijets': histlist[0], 'ReducedHbb': histlist[1]}
	a_file = open("histdic_DL1r_Reduced.json", "w")
	json.dump(histdic, a_file)
	a_file.close()

	print(len(histlist[0][0]))
	print(histlist[0][0])
	for i in range(0,9):
		hist_DL1r = [histlist[0][i], histlist[1][i]]
		draw_hist(hist_DL1r, bins, features[i], "/eos/user/o/okarkout/plots/", 'Reduced_DL')
	# draw_hist(total, bin_centers, errors, "/eos/user/o/okarkout/plots/", file_name="rnd_top")

if __name__ == '__main__':
	run()


