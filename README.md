# Mass-decorrelated-Xbb-tagger
Tools for training and study of mass de-correlated Xbb tagger using deep neural network
by Osama Karkout osamakarkout@gmail.
This is a fork of Wei Ding's [original repo](https://gitlab.cern.ch/ding/massdecorrelatedxbbtagger).

The instructions for how to run are in the file run.sh in each directory.
This tool is inherited from the [tool by Julian Collado Umana](https://gitlab.cern.ch/atlas-boosted-hbb/xbb-tagger-training).
Here are the input MC samples: [https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets).
More studies of this tagger are found [https://indico.cern.ch/event/864911/](https://indico.cern.ch/event/864911/). 

A validation pacakge, [https://gitlab.cern.ch/atlas-boosted-xbb/xbb-validation/](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-validation/) is not at all integrated to this package but has some very useful tools!


# Download files
for the first R&D run I used:
rucio download the datasets listed here [p3990 v3](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p3990/mc16d-h5-v3-datasets.txt) and original dataset names are [here](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p3990/mc16d-input-datasets.txt).

To train of a full dataset:
the datasets are listed here [p4238 v3](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p4258/mc16d-xbbv3-h5-datasets.txt) and the original names are [here](https://gitlab.cern.ch/atlas-boosted-xbb/xbb-datasets/-/blob/master/p4258/mc16d-input-datasets.txt).

## Jets
To quickly R&D JZ3W (361023) to JZ9W (361029) are enough.
`mkdir DijetsDatasets` and put the datasets in there (or softlink)

## Higgs
To quickly R&D use the SM boosted Higgs sample 309450.
`mkdir HbbDatasets` and put the datasets in there (or softlink)

# Run Reweight
Go to the directory, from the base dir, `cd reweight`.

Some helper scripts exist in `/massdecorrelatedxbbtagger/plot_and_study` to explore files.

## Dijet Weights
This step is depricated since we don't use weights for the training. However, this version still gives the option of running weighted training. You can also use weights if you'd like to see smooth QCD distributions.
First run `calculatedDijetsWeights.py` to get the sample weights needed to combine the dijet samples into a smooth distributions accounting for cross-section and filter efficiencies.
```
python calculatedDijetsWeights.py --path path/to/DijetsDatasets/ (include DijetDatasets in the path)
```
Part of the output to the screen is needed in the next script `labelDijetsDatasets.py`. It is the set of numbers in curly brackets `{}` and should be assigned to `listSection`.

## Run Label
The purpose of this step is to create the Reduced h5 files including fatjet variables, variables for subjets 1, 2, and 3, total event weight and a label of the process.
This takes us from files where each entry is an event to files where each entry is a fatjet.

There are three, and each is designed to take the same path. Therefore, the path should not include the directory with a name reflecting the sample content. For example if the dijet datasets are in `/eos/user/s/student/samples/DijetDatasets/` then the path to use for all these scripts is `/eos/user/s/student/samples/`.

These scripts pull as subset of information from the original h5. It currently using DL1r (not DLrT) and saves quite a few JSS variables in case one wants to use them, but they aren't used by default.

**To check - seg fault when saving eventNumber?**

### Jets
```
python labelDijetsDatasets.py --path path/to/samples
```
The output goes to `../../ReducedDijets/`. The output is quite big, so it would be good to softlink and eos diectory to the output directory or to change the output directory to point to your eos directory.

This is time consuming so best let it run for a while. To run on one DISD use `--dsid 3610XX`. Parallelize by running 1 DSID per shell might be needed. Also, to speed things up, a limited number of h5 files can be used from each dataset, but that has to be consistently done from the start.

### Hbb and Top
```
python labelHbbDatasets.py --path path/to/samples
```
It expects there to be two directories inside path/to/samples with the names `HbbDatasets`and `TopDatasets`.
The output goes to `../../ReducedHbb/` and `../../ReducedTop/`. The output is quite big, so it would be good to softlink and eos diectory to the output directory or to change the output directory to point to your eos directory.

** Does the original h5 only consider the leading jet in the event? Seems like 1/2 of the Hbb events are lost which is compatible with only looking at the leading jet **

__labelHbbDatasets is only a few lines different from labelDijetDatasets__

## Run Combine

The code combine_flexible.py produces a training file from the reduced files: ../../Reduced{Dijets,Top,Hbb}. It first finds the resampling information by running a loop on all the files. Then in another loop it processes each file, and stacks the output into a merged dataset. After the loop, we standardize the dataset by shifting by the mean and scaling by the std for all kenimatic variables. The last step is to split the samples into three catagories for: training, validation, and testing.
Processing each file in a loop was done to avoid using too much memory on the lxplus interactive node and memory allocation errors. Resampling significantly reduces the dataset size and so the variable saved within the python code outside the loop is not too large for lxplus to handle.

Here's a little explanation on each step:

1. find_resample_info: Resampling is done to match the eta and pt distributions of Hbb and background {Dijets, Top}. The idea is that we do not want the nn to learn on pt and eta distributions, otherwise it will sculp the background distributions when it applies the bg rejection (pt is correlated with the mass).
Technically, this is done by binning the distributions and comparing the three processes {Hbb, Dijets, Top} in each bin. The lowest bins define a target distribution, and we remove jets within that bin from each process to match the target distribution. To make sure we don't affect other variables that in principle are not correlated to pt nor eta, we remove these jets randomly by assigning a probability to each bin of keeping a jet. This probability is found by dividing the size of the target bin by the size of the bin.

As we process file by file we cannot know the resampling probabilities, which are global and calculated only after counting jets from all files. For this, we first run on all files to calculate resampling info, and then we use them in another loop to process files.

2. getvals: This function does the first layer of processing. First, it applies a pt cut on fatjets and a cut on fatjets without subjets or with subjets whose relativeDeltaRToVRJet is less than a set cutoff relDrMin (flavor-tagging becomes unreliable for these subjets). The default is demanding that all subjets have relativeDeltaRToVRJet above the cutoff, and therefor no collinear subjets (max_col = 0).
We then flag overlapping subjets using the same criteria by setting their DL1r_p{b,c,u}_{1,2,3} variables to nan.
The last step is to calculate binned and unbinned DL1r variables and add them to the data. In the default version these are not used for the training.

3. resample2d: This is done to resample pt and eta distributions as described before.

4. label: training needs to know the correct process; is the jet Hbb, Dijet, or Top. This is done by adding a 3-components unit vector to each fatjet. Each of the 3 unit vectors defines a process.

5. standardize: this shifts all the training variables by thier mean distribution, and scales them by their standard deviation (std). The result are distributions with mean 0 and std = 1. This helps stabelize the training so we see less validation loss fluctuation, and accelerates learning. At the end, np.nan_to_num is applied. This changes all nan values to zero. running the training with nan doesn't work. standardize is applied outside the loop, since it needs global information (mean, std).

6. splitdata: this splits the data into training (70%), validation (15%) and testing (15%) samples. It then appends the data into the output file. In the case of memory allocation problems, one can comment out (standardize) and use these functions within the processing loop to treat file-by-file instead of saving data from all files together. standardize could then be applied to the output file by running it as a condor job using the function (std_from_file).
As a compromise one could also do away with standardization, give the NN the decimal log of pt instead of pt, and train for more epochs.

## Run train.py
The default training does not use the weights. From train/submittingjobs one can submit a job to condor by editing the files g4.sub and submit_job.sh then running condor_submit g4.sub.

## Validation
To give the trained network to the TDD, you'll need to have the network saved as json. To get this network file you can follow the [LWTNN Convertion](https://umami-docs.web.cern.ch/trainings/LWTNN-conversion/) instructions. you'll need the weights.h5 file and architecture.json file, which you get from the training output. In addition, variables_file.json is needed, but not produced by this code. You can simply take the one provided in this git directory and modify it. Make sure you keep the Pt units in MeV, and note that scale = 1/std.
